package test;

import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.packager.PLNPackager;
import com.trimindi.switching.rajabiller.response.MethodResponse;
import com.trimindi.switching.response.pulsa.Payment;
import com.trimindi.switching.services.TransaksiService;
import com.trimindi.switching.utils.TLog;
import com.trimindi.switching.utils.constanta.ResponseCode;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPrePaid;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.List;
import java.util.Objects;

/**
 * Created by PC on 6/22/2017.
 */
public class TransaksiAndDeposit {
    @Test
    public void test(){
        TransaksiService transaksiService = new TransaksiService();

        Transaksi transaksi = transaksiService.findById("D455F630C5584992B0DFD5E6ECD95C71");
        ISOMsg isoMsg = new ISOMsg();
        isoMsg.setPackager(new PLNPackager());
        try {
            isoMsg.unpack(transaksi.getPAYMENT().getBytes());
        } catch (ISOException e) {
            e.printStackTrace();
        }
        System.out.println("legth " + isoMsg.getString(48).length());
        List<Rules> r = new SDE.Builder().setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(48,true))
                .setPayload(isoMsg.getString(48)).generate();
        System.out.println(r);
        int le = new SDE.Builder().setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(48,true))
                .setPayload(isoMsg.getString(48)).calculate();
        System.out.println(le);


    }

    @Test
    public void paymentPulsa(){
        JAXBContext jc;
        Unmarshaller unmarshaller;
        try {
            jc = JAXBContext.newInstance(MethodResponse.class);
            unmarshaller = jc.createUnmarshaller();
            String response = "<methodResponse>\n" +
                    "<params>\n" +
                    "<param>\n" +
                    "<value><array>\n" +
                    "<data>\n" +
                    "<value><string>S5H</string></value>\n" +
                    "<value><string>20170622112715</string></value>\n" +
                    "<value><string>081336643668</string></value>\n" +
                    "<value><string>SP90212</string></value>\n" +
                    "<value><string>974048</string></value>\n" +
                    "<value><string></string></value>\n" +
                    "<value><string>80667A941F7B4B46BFE1C629AF1B22E2</string></value>\n" +
                    "<value><string>749534077</string></value>\n" +
                    "<value><string></string></value>\n" +
                    "<value><string>Pengisian pulsa S5H Anda ke nomor 081336643668 sedang diproses</string></value>\n" +
                    "<value><string>6100</string></value>\n" +
                    "<value><string>4954584</string></value>\n" +
                    "</data>\n" +
                    "</array></value>\n" +
                    "</param>\n" +
                    "</params>\n" +
                    "</methodResponse>";
            MethodResponse methodResponse = (MethodResponse) unmarshaller.unmarshal(new StringReader(response));
            Payment payment = new Payment(methodResponse);
            if(Objects.equals(payment.getSTATUS(), "")){
                System.out.println("empty");
                payment.setSTATUS("0");
            }

            if(Integer.parseInt(payment.getSTATUS()) == 0){
                System.out.println(true);
            }

        } catch (JAXBException e) {
            TLog.log("ERRR" + e.getMessage());
        }


    }
}
