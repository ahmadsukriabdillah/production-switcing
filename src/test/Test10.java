package test;

import com.trimindi.switching.controllers.Request;
import com.trimindi.switching.response.prepaid.InquiryResponse;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PC on 19/07/2017.
 */
public class Test10 {


    Client client;
    WebTarget webTarget;
    MultivaluedMap<String,Object> header;
    List<Request> requestList;

    @Before
    public void init(){
        requestList = new ArrayList<>();
        header = new MultivaluedHashMap<>();
        header.add("userid","TRY0001");
        header.add("time","20170615200635");
        header.add("sign","f0634b3f80730f612dffb83b5eaf8ba537e67ed3");
        header.add("accept","application/json");
        header.add("Content-Type","application/json");
        client = ClientBuilder.newClient();
        webTarget = client.target("http://103.15.226.112:7777/");
        for(int i=1;i<=20;i = i + 2){
            Request request = new Request();
            request.ACTION = "PLN.INQUIRY";
            request.PRODUCT = "BL20";
            request.MSSIDN = "5100000000" + i;
            requestList.add(request);
        }
    }

    @Test
    public void prepaidTest () throws InterruptedException {


        /**
         * INQUIRY
         */
        for(int i=0;i<requestList.size();i++){
            InquiryResponse inquiryResponse = webTarget.request(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .headers(header)
                    .post(Entity.entity(requestList.get(i),MediaType.APPLICATION_JSON),InquiryResponse.class);
            System.out.println(inquiryResponse);
            requestList.get(i).ACTION = "PLN.PAYMENT";
            requestList.get(i).NTRANS = inquiryResponse.getNtrans();
            Thread.sleep(1000);

        };

        for(int i=0;i<requestList.size();i++){
            String paymentResponse = webTarget.request(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .headers(header)
                    .post(Entity.entity(requestList.get(i),MediaType.APPLICATION_JSON),String.class);
            System.out.println(paymentResponse);
            Thread.sleep(1000);
        }
    }
}
