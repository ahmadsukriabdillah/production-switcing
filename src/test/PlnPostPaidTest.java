package test;

import com.trimindi.switching.controllers.Request;
import com.trimindi.switching.response.postpaid.InquiryResponse;
import com.trimindi.switching.response.postpaid.PaymentResponse;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by PC on 6/21/2017.
 */
public class PlnPostPaidTest {

    Client client;
    WebTarget webTarget;
    MultivaluedMap<String,Object> header;

    @Before
    public void init(){
        header = new MultivaluedHashMap<>();
        client = ClientBuilder.newClient();
        webTarget = client.target("http://127.0.0.1:7777/");
    }

    @Test
    public void prepaidTest (){
        Request request = new Request();
        request.ACTION = "PLN.INQUIRY";
        request.PRODUCT = "PLNPP";
        request.MSSIDN = "52000000001";

        header.add("userid","TRY0001");
        header.add("time","20170615200635");
        header.add("sign","f0634b3f80730f612dffb83b5eaf8ba537e67ed3");
        header.add("accept","application/json");
        header.add("Content-Type","application/json");


        /**
         * INQUIRY
         */
        InquiryResponse inquiryResponse = webTarget.request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .headers(header)
                .post(Entity.entity(request,MediaType.APPLICATION_JSON),InquiryResponse.class);
        System.out.println(inquiryResponse);
        request.ACTION = "PLN.PAYMENT";
        request.NTRANS = inquiryResponse.getNtrans();
        /**
         * PAYMENT
         */
        PaymentResponse paymentResponse = webTarget.request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .headers(header)
                .post(Entity.entity(request,MediaType.APPLICATION_JSON),PaymentResponse.class);
        System.out.println(paymentResponse);

        /**
         * CETAK STRUK
         */
        request.ACTION = "PLN.CETAK.ULANG";
        PaymentResponse cetakstruk = webTarget.request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .headers(header)
                .post(Entity.entity(request,MediaType.APPLICATION_JSON),PaymentResponse.class);
        System.out.println(paymentResponse);
    }
}
