package test;

import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.rajabiller.RajabilerRequest;
import com.trimindi.switching.services.TransaksiService;
import com.trimindi.switching.utils.generator.BaseHelper;
import com.trimindi.switching.utils.generator.PrePaidGenerator;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by PC on 6/6/2017.
 */
public class PrepaidResponseParsingTest {
    TransaksiService transaksiService;

    @Before
    public void init(){
        transaksiService = new TransaksiService();
    }

    @Test
    public void pulsa(){
        transaksiService = new TransaksiService();
        Transaksi r = transaksiService.findById("0882ED1FC9B84696B0087DB10316A21E");
        String res = RajabilerRequest.paymentTelokm(r);
        System.out.println(res);
    }


    @Test
    public void parsingresponse(){
        String response = "0000000530000000004404D1A3E09D702797935DEF6A36CF133390SUBCRIBER NAME           00005000000000000015  R10000013000000100002017030603201706032017000000300000C000000000000000000000000000000000000111100002222000000080000000800000008000000082017040604201706042017000000600000C000000000000000000000000000000000000111100002222000000080000000800000008000000082017050605201706052017000000900000C000000000000000000000000000000000000111100002222000000080000000800000008000000082017060606201706062017000001200000C00000000000000000000000000000000000011110000222200000008000000080000000800000008";
        List<Rules> rulesList = new SDE.Builder().setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48,true))
                .setPayload(response)
                .generate();
        int count = new SDE.Builder().setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48,true))
                .setPayload(response)
                .calculate();

        System.out.println("jumlah" + count);
        System.out.println(rulesList);

    }

    @Test
    public void testtime() throws ParseException {
        Date time = new SimpleDateFormat("yyyyMMddHHmmss").parse("20170615060225");

        System.out.println(time.toString());
    }

    @Test
    public void stantest() throws ISOException {

        ISOMsg isoMsg = PrePaidGenerator.generateNetworkSignOn();
            System.out.println(new String(isoMsg.pack()));
    }

    @Test
    public void NTRANS(){

    }
}
