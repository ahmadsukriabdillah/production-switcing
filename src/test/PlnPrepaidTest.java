package test;

import com.trimindi.switching.MainClass;
import com.trimindi.switching.controllers.Request;
import com.trimindi.switching.q2.Deploy;
import com.trimindi.switching.response.prepaid.InquiryResponse;
import com.trimindi.switching.response.prepaid.PaymentResponse;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Created by PC on 6/21/2017.
 */
public class PlnPrepaidTest {

    Client client;
    WebTarget webTarget;
    MultivaluedMap<String,Object> header;

    @Before
    public void init(){
        header = new MultivaluedHashMap<>();
        client = ClientBuilder.newClient();
        webTarget = client.target("http://127.0.0.1:7777/");
    }

    @Test
    public void prepaidTest (){
        Request request = new Request();
        request.ACTION = "PLN.INQUIRY";
        request.PRODUCT = "BL20";
        request.MSSIDN = "530000000001";

        header.add("userid","TRY0001");
        header.add("time","20170615200635");
        header.add("sign","f0634b3f80730f612dffb83b5eaf8ba537e67ed3");
        header.add("accept","application/json");
        header.add("Content-Type","application/json");

        /**
         * INQUIRY
         */
        InquiryResponse inquiryResponse = webTarget.request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .headers(header)
                .post(Entity.entity(request,MediaType.APPLICATION_JSON),InquiryResponse.class);
        System.out.println(inquiryResponse);

        /**
         * PAYMENT
         */
        request.ACTION = "PLN.PAYMENT";
        request.NTRANS = inquiryResponse.getNtrans();
        PaymentResponse paymentResponse = webTarget.request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .headers(header)
                .post(Entity.entity(request,MediaType.APPLICATION_JSON),PaymentResponse.class);
        System.out.println(paymentResponse);

        /**
         * CETAK ULANG
         *
         */
        request.ACTION = "PLN.CETAK.ULANG";
        PaymentResponse CETAK = webTarget.request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .headers(header)
                .post(Entity.entity(request,MediaType.APPLICATION_JSON),PaymentResponse.class);
        System.out.println(CETAK);

    }
}
