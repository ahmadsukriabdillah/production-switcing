package test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.servindo.PaketData;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

/**
 * Created by PC on 21/07/2017.
 */
public class ServindoTest {

    private HttpGet httpGet;
    private CloseableHttpClient client;
    private URIBuilder builder;
    private String host = "127.0.0.1";
    private int port = 80;
    private ObjectMapper objectMapper;

    @Test
    public void test() throws URISyntaxException {
        objectMapper = new ObjectMapper();
        client = HttpClients.custom()
                .build();
        builder = new URIBuilder();
        builder.setScheme("http").setHost(host).setPort(port).setPath("servindo/")
                .setParameter("reg_id","1982378912738129731289" )
                .setParameter("userid", "TRIMINDI")
                .setParameter("passwd", "SADASD")
                .setParameter("msisd", "085232906724")
                .setParameter("denom","10");
        httpGet = new HttpGet(builder.build());
        try {
            CloseableHttpResponse response = client.execute(httpGet);
            String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            PaketData paketData;
            paketData = objectMapper.readValue(respone,PaketData.class);
            System.out.println(paketData);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
