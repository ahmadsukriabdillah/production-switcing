package com.trimindi.switching.filter;

import com.trimindi.switching.models.PartnerCredential;
import com.trimindi.switching.models.PartnerPermision;
import com.trimindi.switching.services.PartnerCredentialService;
import com.trimindi.switching.services.PartnerPermisionService;
import com.trimindi.switching.utils.constanta.Constanta;
import com.trimindi.switching.utils.constanta.ResponseCode;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by sx on 13/05/17.
 */

public class SecureEndpoint implements ContainerRequestFilter {

    @Context
    private HttpServletRequest sr;

    PartnerCredentialService partnerCredentialService;
    PartnerPermisionService partnerPermisionService;
    private static Map<String,String> whitelist;

    private static Map<String,String> whiteListIPAddress = new HashMap<>();
    static {
        Map<String,String> w = new HashMap<>();
        w.put("partner/version","partner/version");
        w.put("servindo/report","servindo/report");
        whitelist = Collections.unmodifiableMap(w);
    }
    public SecureEndpoint() {
        partnerCredentialService = new PartnerCredentialService();
        partnerPermisionService = new PartnerPermisionService();

    }

    @Override
    public void filter(ContainerRequestContext req) throws IOException {
        Response SERVER_UNAUTORIZED = Response.status(200).entity(new ResponseCode("9997","UNAUTHORIZATION REQUEST")).build();
        Response SERVER_IP_BLOCK = Response.status(200).entity(new ResponseCode("9977","UNREGISTERED IP ADDRESS.")).build();

        String scheme = req.getUriInfo().getPath();
        if(whitelist.containsKey(scheme)){

        }else{
            String userid = req.getHeaderString("userid");
            String sign = req.getHeaderString("sign");
            String time = req.getHeaderString("time");
            if(userid == null || sign == null || time == null){
                req.abortWith(SERVER_UNAUTORIZED);
                return;
            }
            if(userid.isEmpty() || sign.isEmpty() || time.isEmpty()){
                req.abortWith(SERVER_UNAUTORIZED);
                return;
            }
            req.getHeaders().putSingle("Content-Type",MediaType.APPLICATION_JSON);
            PartnerCredential partnerCredential = partnerCredentialService.findByUserId(userid);
            if(partnerCredential != null){
                if(partnerCredential.getIp_address().equalsIgnoreCase("*") || partnerCredential.getIp_address().equalsIgnoreCase(sr.getRemoteAddr())){
                    String pass = DigestUtils.sha1Hex(partnerCredential.getPartner_uid() + time + partnerCredential.getPartner_password() + partnerCredential.getMerchant_type());
                    if(sign.equals(pass)){
                        PartnerPermision partnerPermision = partnerPermisionService.getPermision(partnerCredential);
                        req.setProperty(Constanta.MAC , req.getHeaderString(Constanta.MAC));
                        req.setProperty(Constanta.IP_ADDRESS , sr.getRemoteAddr());
                        req.setProperty(Constanta.PRINCIPAL , partnerCredential);
                        req.setProperty(Constanta.PERMISION, partnerPermision);
                        req.setProperty(Constanta.LATITUDE,req.getHeaderString(Constanta.LATITUDE));
                        req.setProperty(Constanta.LONGITUDE,req.getHeaderString(Constanta.LONGITUDE));
                    }else {
                        req.abortWith(SERVER_UNAUTORIZED);
                    }
                }else{
                    req.abortWith(SERVER_IP_BLOCK);
                }
            }else{
                req.abortWith(SERVER_UNAUTORIZED);
            }
        }
    }
}