package com.trimindi.switching.exeption;

import com.fasterxml.jackson.core.JsonParseException;
import com.trimindi.switching.utils.constanta.ResponseCode;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by PC on 6/17/2017.
 */
public class CustomNotFoundExeption implements ExceptionMapper<NotFoundException> {

    @Override
    public Response toResponse(NotFoundException e) {
        e.printStackTrace();
        return Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build();
    }
}
