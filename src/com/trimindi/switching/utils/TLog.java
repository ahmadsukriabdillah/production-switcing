package com.trimindi.switching.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by HP on 18/05/2017.
 */
public class TLog {
    public TLog() {
    }

    public static void log(String s) {
        try {
            String filename = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            File logFile = new File("log/"+filename+".log");
            FileWriter fw = new FileWriter(logFile,true);
            String date = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss").format(new Date());
            fw.write(date+" : "+s);
            fw.write(System.lineSeparator());
            fw.close();
        } catch (IOException ex) {
            System.err.println("Couldn't log this: "+s);
        }
    }
}
