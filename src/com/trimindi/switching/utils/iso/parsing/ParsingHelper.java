package com.trimindi.switching.utils.iso.parsing;

import com.trimindi.switching.utils.TLog;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorNonTagList;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPrePaid;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.jpos.iso.ISOMsg;

import java.util.List;

/**
 * Created by HP on 23/05/2017.
 */
public class ParsingHelper {
    public static List<Rules> parsingRulesPostPaid(ISOMsg d, boolean status) {
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(d.getString(48))
                .setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, status))
                .generate();
        bit48.add(new Rules(d.getString(63)));
        return bit48;
    }
    public static List<Rules> parsingRulesPrepaid(ISOMsg d, boolean status) {
        List<Rules> bit48 = null;
        try{
            bit48 = new SDE.Builder().setPayload(d.getString(48))
                    .setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(48,status))
                    .generate();
            List<Rules> bit62 = new SDE.Builder()
                    .setPayload(d.getString(62))
                    .setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(62,status))
                    .generate();
            bit48.addAll(bit62);
            bit48.add(new Rules(d.getString(63)));
            return bit48;
        }catch (Exception e){
            e.printStackTrace();
        }
        return bit48;
    }
    public static List<Rules> parsingRulesNontaglist(ISOMsg d, boolean status) {
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(d.getString(48))
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListPaymentResponse(48, status))
                .generate();
        List<Rules> bit61 = new SDE.Builder()
                .setPayload(d.getString(61))
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListPaymentResponse(61, status))
                .generate();
        List<Rules> bit62 = new SDE.Builder()
                .setPayload(d.getString(62))
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListPaymentResponse(62, status))
                .generate();
        bit48.addAll(bit61);
        bit48.addAll(bit62);
        bit48.add(new Rules(d.getString(63)));
        return bit48;
    }
}
