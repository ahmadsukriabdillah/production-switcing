package com.trimindi.switching.utils.iso.builder;

import com.trimindi.switching.utils.iso.models.IsoField;
import com.trimindi.switching.packager.PLNPackager;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 15/05/2017.
 */
public class ISOMsgBuilder {
    public static class Builder{
        private List<IsoField> list;
        private String MIT;
        private ISOPackager packager;
        public Builder(String MIT){
            this.MIT = MIT;
            this.packager = new PLNPackager();
            this.list = new ArrayList<>();
        }
        public Builder addPackager(ISOPackager packager){
            this.packager = packager;
            return this;
        }
        public Builder addField(IsoField isoField){
            this.list.add(isoField);
            return this;
        }
        public Builder addField(int index,String value){
            this.list.add(new IsoField(value,index));
            return this;
        }
        public ISOMsg build() throws ISOException {
            ISOMsg msg = new ISOMsg();
            msg.setMTI(MIT);
            msg.setPackager(packager);
            for (IsoField f : list) {
                msg.set(f.getIndex(),f.getValue());
            }
            return msg;
        }
    }
}
