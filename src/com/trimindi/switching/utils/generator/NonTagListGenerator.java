package com.trimindi.switching.utils.generator;

import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.packager.PLNPackager;
import com.trimindi.switching.response.nontaglist.Inquiry;
import com.trimindi.switching.utils.iso.builder.FieldBuilder;
import com.trimindi.switching.utils.iso.builder.ISOMsgBuilder;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorNonTagList;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import java.util.List;

/**
 * Created by HP on 15/05/2017.
 */
public class NonTagListGenerator extends BaseHelper {
    private static final String AREA_CODE = "00";
    private static final String PAN_NONTAGLIST = "99504";


    public static ISOMsg generateInquiry(String MERCHANT_CODE, String REGISTRATION_ID) throws ISOException {
        ISOMsg msg = new ISOMsgBuilder.Builder("2100")
                .addPackager(new PLNPackager())
                .addField(2,PAN_NONTAGLIST)
                .addField(11,Stan())
                .addField(12,date14())
                .addField(26,MERCHANT_CODE)
                .addField(32,BANK_CODE)
                .addField(33,PARTNER_ID)
                .addField(41,TERMINAL_ID)
                .addField(48,new FieldBuilder.Builder()
                        .addValue(SWITCHER_ID,7,"0","L")
                        .addValue(REGISTRATION_ID,32," ","R")
                        .addValue(AREA_CODE,2,"0","L")
                        .addValue("",3,"0","R")
                        .build())
                .build();
        return msg;
    }

    public static ISOMsg generatePurchase(Transaksi transaksi) throws ISOException {
        ISOMsg inquiry = new ISOMsg();
        inquiry.setPackager(new PLNPackager());
        inquiry.unpack(transaksi.getINQUIRY().getBytes());
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(inquiry.getString(48))
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListInquiryResponse(48, true))
                .generate();
        List<Rules> bit62 = new SDE.Builder()
                .setPayload(inquiry.getString(62))
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListInquiryResponse(62, true))
                .generate();
        bit48.addAll(bit62);
        Inquiry inquiryResponse = new Inquiry(bit48,true);
        ISOMsg msg = new ISOMsgBuilder.Builder("2200")
                .addPackager(new PLNPackager())
                .addField(2,PAN_NONTAGLIST)
                .addField(4, amountToPay(transaksi.getAMOUT() + transaksi.getADMIN()))
                .addField(11,inquiry.getString(11))
                .addField(12,date14())
                .addField(26, transaksi.getMERCHANT_ID())
                .addField(32,BANK_CODE)
                .addField(33,PARTNER_ID)
                .addField(41,TERMINAL_ID)
                .addField(48,new FieldBuilder.Builder()
                        .addValue(SWITCHER_ID,7,"0","L")
                        .addValue(inquiryResponse.getRegistrationNumber(),32," ","R")
                        .addValue(inquiryResponse.getAreaCode(),2,"0","L")
                        .addValue(inquiryResponse.getTransactionCode(),3,"0","L")
                        .addValue(inquiryResponse.getTransactionName(),25," ","R")
                        .addValue(inquiryResponse.getRegistrationDate(),8," ","R")
                        .addValue(inquiryResponse.getExpirationDate(),8," ","R")
                        .addValue(inquiryResponse.getSubscriberID(), 12, "0", "L")
                        .addValue(inquiryResponse.getSubscriberName(), 25, " ", "R")
                        .addValue(inquiryResponse.getPLNReferenceNumber(),32," ","R")
                        .addValue(inquiryResponse.getBukopinReferenceNumber(),32," ","R")
                        .addValue(inquiryResponse.getServiceUnit(),5,"0","L")
                        .addValue(inquiryResponse.getServiceUnitAddress(),35," ","R")
                        .addValue(inquiryResponse.getServiceUnitPhone(),15," ","R")
                        .addValue(String.valueOf(inquiryResponse.getTotalTransactionAmountMinorUnit()),1,"0","L")
                        .addValue(valueToMinor(inquiryResponse.getTagihan() + inquiryResponse.getAdmin(), inquiryResponse.getTotalTransactionAmountMinorUnit()), 17, "0", "L")
                        .addValue(String.valueOf(inquiryResponse.getPLNBILLMinorUnit()),1,"0","L")
                        .addValue(valueToMinor(inquiryResponse.getPLNBILLValue(),inquiryResponse.getPLNBILLMinorUnit()),17,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getAdministrationChargeMinorUnit()),1,"0","L")
                        .addValue(valueToMinor(inquiryResponse.getAdmin(),inquiryResponse.getAdministrationChargeMinorUnit()),10,"0","L")
                        .build())
                .addField(62,new FieldBuilder.Builder()
                        .addValue(inquiryResponse.getBillcomponenttype(),2,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getBillComponentMinorUnit()),1,"0","L")
                        .addValue(valueToMinor(inquiryResponse.getBillComponentValueAmount(),inquiryResponse.getBillComponentMinorUnit()),17,"0","L")
                        .build())
                .build();

        return msg;
    }

    public static ISOMsg generateReversal(ISOMsg isoMsg, String MTI) throws ISOException {
        isoMsg.setPackager(new PLNPackager());

        ISOMsg newMsg = (ISOMsg) isoMsg.clone();
        newMsg.setPackager(new PLNPackager());
        newMsg.setMTI(MTI);
        newMsg.set(12, date14());
        newMsg.set(56, new FieldBuilder.Builder()
                .addValue(isoMsg.getMTI(), 4, "0", "L")
                .addValue(isoMsg.getString(11), 12, "0", "L")
                .addValue(isoMsg.getString(12), 14, "0", "L")
                .addValue(isoMsg.getString(32), 7, "0", "L")
                .build());
        return newMsg;
    }
}
