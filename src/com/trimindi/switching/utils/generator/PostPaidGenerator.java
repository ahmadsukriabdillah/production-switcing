package com.trimindi.switching.utils.generator;

import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.packager.PLNPackager;
import com.trimindi.switching.response.postpaid.Inquiry;
import com.trimindi.switching.response.postpaid.Rincian;
import com.trimindi.switching.utils.iso.builder.FieldBuilder;
import com.trimindi.switching.utils.iso.builder.ISOMsgBuilder;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 15/05/2017.
 */
public class PostPaidGenerator extends BaseHelper {

    public static ISOMsg generateReversal(ISOMsg isoMsg, String MTI) throws ISOException {
        isoMsg.setPackager(new PLNPackager());

        ISOMsg newMsg = (ISOMsg) isoMsg.clone();
        newMsg.setPackager(new PLNPackager());
        newMsg.setMTI(MTI);
//        newMsg.set(11, Stan());
        newMsg.set(12, date14());
        newMsg.set(56, new FieldBuilder.Builder()
                .addValue(isoMsg.getMTI(), 4, "0", "L")
                .addValue(isoMsg.getString(11), 12, "0", "L")
                .addValue(isoMsg.getString(12), 14, "0", "L")
                .addValue(isoMsg.getString(32), 7, "0", "L")
                .build());
        newMsg.unset(61);
        return newMsg;
    }

    public static ISOMsg generatePurchase(Transaksi t) throws ISOException {

        String payload = t.getINQUIRY();
        ISOMsg inquiry = new ISOMsg();
        inquiry.setPackager(new PLNPackager());
        inquiry.unpack(payload.getBytes());
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(inquiry.getString(48))
                .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48,true))
                .generate();

        Inquiry inquiryResponse = new Inquiry(bit48,true);
        FieldBuilder.Builder bit48compose = new FieldBuilder.Builder()
                .addValue(SWITCHER_ID,7,"","L")
                .addValue(t.getMSSIDN(),12,"0","L")
                .addValue(String.valueOf(inquiryResponse.getBillStatus()),1,"0","R")
                .addValue(String.valueOf(inquiryResponse.getBillStatus()),1,"0","R")
                .addValue(String.valueOf(inquiryResponse.getTotalOutstandingBill()),2,"0","L")
                .addValue(inquiryResponse.getBukopinTbkReferenceNumber(),32," ","R")
                .addValue(inquiryResponse.getSubscriberName(),25," ","R")
                .addValue(inquiryResponse.getSubscriberUnit(), 5, "0", "L")
                .addValue(inquiryResponse.getServiceUnitPhone(), 15, "0", "L")
                .addValue(inquiryResponse.getSubscriberSegmentation(),4," ","R")
                .addValue(String.valueOf(inquiryResponse.getPowerConsumingCategory()),9,"0","L")
                .addValue(new DecimalFormat("#").format(inquiryResponse.getAdmin()),9,"0","L");

        int legth = new SDE.Builder()
                .setPayload(inquiry.getString(48))
                .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48,true))
                .calculate();

        String rincian = inquiry.getString(48).substring(legth,inquiry.getString(48).length());
        List<Rincian> rincians = new ArrayList<>();
        int start = 0;
        int leghtRincian = 115;
        double total = 0;
        double denda = 0;
        for(int i= 0;i<inquiryResponse.getBillStatus();i++){
            String parsRincian = rincian.substring(start,start+leghtRincian);
            List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
            Rincian r = new Rincian(rc);
            rincians.add(r);
            total += r.getTotalElectricityBill();
            denda += r.getPenaltyFee();
            start += leghtRincian;
            bit48compose.addValue(parsRincian,115,"0","L");
        }
        total += denda + inquiryResponse.getAdmin();
        inquiryResponse.setRincian(rincians);
        return new ISOMsgBuilder.Builder("2200")
                .addPackager(new PLNPackager())
                .addField(2, PAN_POSTPAID)
                .addField(4, amountToPay(total))
                .addField(11,inquiry.getString(11))
                .addField(12,date14())
                .addField(15,date8())
                .addField(26, t.getMERCHANT_ID())
                .addField(32,BANK_CODE)
                .addField(33,PARTNER_ID)
                .addField(41,TERMINAL_ID)
                .addField(48, bit48compose.build())
                .addField(61, t.getBILL_REF_NUMBER())
                .build();
    }



    public static ISOMsg generateInquiry(String MACHINE_TYPE, String MSSIDN) throws ISOException {
        return new ISOMsgBuilder.Builder("2100")
                .addPackager(new PLNPackager())
                .addField(2,PAN_POSTPAID)
                .addField(11,Stan())
                .addField(12,date14())
                .addField(26,MACHINE_TYPE)
                .addField(32,BANK_CODE)
                .addField(33,PARTNER_ID)
                .addField(41,TERMINAL_ID)
                .addField(48, new FieldBuilder.Builder()
                        .addValue(SWITCHER_ID,7,"","L")
                        .addValue(MSSIDN,12,"0","R")
                        .addValue("1",1,"0","R")
                        .build())
                .build();
    }
}
