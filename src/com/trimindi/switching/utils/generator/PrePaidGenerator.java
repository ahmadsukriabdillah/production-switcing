package com.trimindi.switching.utils.generator;

import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.packager.PLNPackager;
import com.trimindi.switching.response.prepaid.Inquiry;
import com.trimindi.switching.utils.iso.builder.FieldBuilder;
import com.trimindi.switching.utils.iso.builder.ISOMsgBuilder;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPrePaid;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import java.util.List;

/**
 * Created by sx on 13/05/17.
 */
public class PrePaidGenerator extends BaseHelper {

    public static ISOMsg generateNetworkSignOn() throws ISOException {
        ISOMsg msg = new ISOMsgBuilder.Builder("2800")
                .addPackager(new PLNPackager())
                .addField(12,date14())
                .addField(33,PARTNER_ID)
                .addField(40,SIGN_ON)
                .addField(41,TERMINAL_ID)
                .addField(48,SWITCHER_ID)
                .build();
        return msg;
    }
    public static ISOMsg generateNetworkSignOFF() throws ISOException {
        ISOMsg msg = new ISOMsgBuilder.Builder("2800")
                .addPackager(new PLNPackager())
                .addField(12,date14())
                .addField(33,PARTNER_ID)
                .addField(40,SIGN_OFF)
                .addField(41,TERMINAL_ID)
                .addField(48,SWITCHER_ID)
                .build();
        return msg;
    }

    public static ISOMsg generateNetworkEchoTest() throws ISOException {
        ISOMsg msg = new ISOMsgBuilder.Builder("2800")
                .addPackager(new PLNPackager())
                .addField(12,date14())
                .addField(33,PARTNER_ID)
                .addField(40,ECHO_TEST)
                .addField(41,TERMINAL_ID)
                .addField(48,SWITCHER_ID)
                .build();
        return msg;
    }

    public static ISOMsg generateInquiry(String MERCHANT_CODE, String MSSIDN) throws ISOException {
        ISOMsg msg = new ISOMsgBuilder.Builder("2100")
                .addPackager(new PLNPackager())
                .addField(2,PAN_PREPAID)
                .addField(11,Stan())
                .addField(12,date14())
                .addField(26,MERCHANT_CODE)
                .addField(32,BANK_CODE)
                .addField(33,PARTNER_ID)
                .addField(41,TERMINAL_ID)
                .addField(48,new FieldBuilder.Builder()
                        .addValue(SWITCHER_ID,7,"0","R")
                        .addValue((MSSIDN.length() == 11 ? MSSIDN: "0"),11,"0","R")
                        .addValue((MSSIDN.length() == 12 ? MSSIDN: "0"),12,"0","R")
                        .addValue((MSSIDN.length() == 11 ? "0": "1"),1,"0","R")
                        .build())
                .build();
        return msg;
    }

    public static ISOMsg generatePurchase(Transaksi t) throws ISOException {
        String payload = t.getINQUIRY();
        ISOMsg inquiry = new ISOMsg();
        inquiry.setPackager(new PLNPackager());
        inquiry.unpack(payload.getBytes());
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(inquiry.getString(48))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(48,true))
                .generate();

        List<Rules> bit62 = new SDE.Builder()
                .setPayload(inquiry.getString(62))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(62,true))
                .generate();
        bit48.addAll(bit62);
        Inquiry inquiryResponse = new Inquiry(bit48,true);

        String unsold = "0";
        StringBuilder unsoldResponse = new StringBuilder();
        if(inquiryResponse.getTotalRepeat() > 0){
            String data = inquiry.getString(62);
            int legth = data.length();
            for(int i=0;i<inquiryResponse.getTotalRepeat();i++){
                int newlegth = legth - 11;
                if(t.getAMOUT() == Double.parseDouble(data.substring(newlegth,legth))){
                    unsold = "1";
                }
                unsoldResponse.append(data.substring(newlegth,legth));
                legth = newlegth;
            }
        }


        ISOMsg msg = new ISOMsgBuilder.Builder("2200")
                .addPackager(new PLNPackager())
                .addField(2,PAN_PREPAID)
                .addField(4, amountToPay(t.getAMOUT() + t.getADMIN()))
                .addField(11,inquiry.getString(11))
                .addField(12,date14())
                .addField(26,t.getMERCHANT_ID())
                .addField(32,BANK_CODE)
                .addField(33,PARTNER_ID)
                .addField(41,TERMINAL_ID)
                .addField(48,new FieldBuilder.Builder()
                        .addValue(SWITCHER_ID,7,"","L")
                        .addValue(inquiryResponse.getMeterSerialNumber(),11,"0","R")
                        .addValue(inquiryResponse.getSubscriberID(),12,"0","R")
                        .addValue((t.getMSSIDN().length() == 11? "1" : "0"),1,"0","R")
                        .addValue(inquiryResponse.getPLNReferenceNumber(),32," ","R")//ref pln
                        .addValue(inquiryResponse.getBukopinReferenceNumber(),32," ","R")//ref bukopin
                        .addValue(inquiryResponse.getSubscriberName(),25," ","R")//subscribe name
                        .addValue(inquiryResponse.getSubscriberSegmentation(),4," ","R") //subscribe segement
                        .addValue(String.valueOf(inquiryResponse.getPowerConsumingCategory()),9,"0","L") //power consumstion
                        .addValue("0",1,"0","R") // minor unit
                        .addValue(numberToPay(inquiryResponse.getAdmin()),10,"0","L") // admin charge
                        .addValue(unsold,1,"0","L")
                        .build())
                .addField(62,new FieldBuilder.Builder()
                        .addValue(inquiryResponse.getDistributionCode(),2," ","R")
                        .addValue(String.valueOf(inquiryResponse.getServiceUnit()),5,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getServiceUnitPhone()),15,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getMaxKWHLimit()),5,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getTotalRepeat()),1,"0","R")
                        .addValue(unsoldResponse.toString(),(inquiryResponse.getTotalRepeat() * 11),"0","L")
                        .build())
                .build();
        return msg;
    }

    public static ISOMsg generatePurchaseAdvice(Transaksi t) throws ISOException {
        String payload = t.getINQUIRY();
        ISOMsg inquiry = new ISOMsg();
        inquiry.setPackager(new PLNPackager());
        inquiry.unpack(payload.getBytes());
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(inquiry.getString(48))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(48,true))
                .generate();

        List<Rules> bit62 = new SDE.Builder()
                .setPayload(inquiry.getString(62))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(62,true))
                .generate();
        bit48.addAll(bit62);
        Inquiry inquiryResponse = new Inquiry(bit48,true);
        String unsold = "0";
        StringBuilder unsoldResponse = new StringBuilder();
        if(inquiryResponse.getTotalRepeat() > 0){
            String data = inquiry.getString(62);
            int legth = data.length();
            for(int i=0;i<inquiryResponse.getTotalRepeat();i++){
                int newlegth = legth - 11;
                if(t.getAMOUT() == Double.parseDouble(data.substring(newlegth,legth))){
                    unsold = "1";
                }
                unsoldResponse.append(data.substring(newlegth,legth));
                legth = newlegth;
            }
        }
        ISOMsg msg = new ISOMsgBuilder.Builder("2220")
                .addPackager(new PLNPackager())
                .addField(2,PAN_PREPAID)
                .addField(4, amountToPay(t.getAMOUT() + t.getADMIN()))
                .addField(11,inquiry.getString(11))
                .addField(12,date14())
                .addField(26,t.getMERCHANT_ID())
                .addField(32,BANK_CODE)
                .addField(33,PARTNER_ID)
                .addField(41,TERMINAL_ID)
                .addField(48,new FieldBuilder.Builder()
                        .addValue(SWITCHER_ID,7,"","L")
                        .addValue(inquiryResponse.getMeterSerialNumber(),11,"0","R")
                        .addValue(inquiryResponse.getSubscriberID(),12,"0","R")
                        .addValue((t.getMSSIDN().length() == 11 ? "0" : "1"),1,"0","R")
                        .addValue(inquiryResponse.getPLNReferenceNumber(),32," ","R")//ref pln
                        .addValue(inquiryResponse.getBukopinReferenceNumber(),32," ","R")//ref bukopin
                        .addValue(inquiryResponse.getSubscriberName(),25," ","R")//subscribe name
                        .addValue(inquiryResponse.getSubscriberSegmentation(),4," ","R") //subscribe segement
                        .addValue(String.valueOf(inquiryResponse.getPowerConsumingCategory()),9,"0","L") //power consumstion
                        .addValue("0",1,"0","R") // minor unit
                        .addValue(numberToPay(inquiryResponse.getAdmin()),10,"0","L") // admin charge
                        .addValue(unsold,1,"0","L")
                        .build())
                .addField(62,new FieldBuilder.Builder()
                        .addValue(inquiryResponse.getDistributionCode(),2," ","R")
                        .addValue(String.valueOf(inquiryResponse.getServiceUnit()),5,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getServiceUnitPhone()),15,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getMaxKWHLimit()),5,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getTotalRepeat()),1,"0","R")
                        .addValue(unsoldResponse.toString(),(inquiryResponse.getTotalRepeat() * 11),"0","L")
                        .build())
                .build();
        return msg;
    }

    public static ISOMsg generatePurchaseAdviceRepeat(Transaksi t) throws ISOException {
        String payload = t.getINQUIRY();
        ISOMsg inquiry = new ISOMsg();
        inquiry.setPackager(new PLNPackager());
        inquiry.unpack(payload.getBytes());
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(inquiry.getString(48))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(48,true))
                .generate();

        List<Rules> bit62 = new SDE.Builder()
                .setPayload(inquiry.getString(62))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(62,true))
                .generate();
        bit48.addAll(bit62);
        Inquiry inquiryResponse = new Inquiry(bit48,true);
        String unsold = "0";
        StringBuilder unsoldResponse = new StringBuilder();
        if(inquiryResponse.getTotalRepeat() > 0){
            String data = inquiry.getString(62);
            int legth = data.length();
            for(int i=0;i<inquiryResponse.getTotalRepeat();i++){
                int newlegth = legth - 11;
                if(t.getAMOUT() == Double.parseDouble(data.substring(newlegth,legth))){
                    unsold = "1";
                }
                unsoldResponse.append(data.substring(newlegth,legth));
                legth = newlegth;
            }
        }
        ISOMsg msg = new ISOMsgBuilder.Builder("2230")
                .addPackager(new PLNPackager())
                .addField(2,PAN_PREPAID)
                .addField(4, amountToPay(t.getAMOUT() + t.getADMIN()))
                .addField(11,inquiry.getString(11))
                .addField(12,date14())
                .addField(26,t.getMERCHANT_ID())
                .addField(32,BANK_CODE)
                .addField(33,PARTNER_ID)
                .addField(41,TERMINAL_ID)
                .addField(48,new FieldBuilder.Builder()
                        .addValue(SWITCHER_ID,7,"","L")
                        .addValue(inquiryResponse.getMeterSerialNumber(),11,"0","R")
                        .addValue(inquiryResponse.getSubscriberID(),12,"0","R")
                        .addValue((t.getMSSIDN().length() == 11? "0":"1"),1,"0","R")
                        .addValue(inquiryResponse.getPLNReferenceNumber(),32," ","R")//ref pln
                        .addValue(inquiryResponse.getBukopinReferenceNumber(),32," ","R")//ref bukopin
                        .addValue(inquiryResponse.getSubscriberName(),25," ","R")//subscribe name
                        .addValue(inquiryResponse.getSubscriberSegmentation(),4," ","R") //subscribe segement
                        .addValue(String.valueOf(inquiryResponse.getPowerConsumingCategory()),9,"0","L") //power consumstion
                        .addValue("0",1,"0","R") // minor unit
                        .addValue(numberToPay(inquiryResponse.getAdmin()),10,"0","L") // admin charge
                        .addValue(unsold,1,"0","L")
                        .build())
                .addField(62,new FieldBuilder.Builder()
                        .addValue(inquiryResponse.getDistributionCode(),2," ","R")
                        .addValue(String.valueOf(inquiryResponse.getServiceUnit()),5,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getServiceUnitPhone()),15,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getMaxKWHLimit()),5,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getTotalRepeat()),1,"0","R")
                        .addValue(unsoldResponse.toString(),(inquiryResponse.getTotalRepeat() * 11),"0","L")
                        .build())
                .build();
        return msg;
    }

}
