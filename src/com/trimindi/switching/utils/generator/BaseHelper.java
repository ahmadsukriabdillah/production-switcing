package com.trimindi.switching.utils.generator;

import com.trimindi.switching.SessionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;

import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by sx on 13/05/17.
 */
public class BaseHelper {
    public static final String PAN_PREPAID = "99502";
    public static final String PAN_POSTPAID = "99501";
    public static final String PAN_NONTAGLIST = "99504";

    public static final String PARTNER_ID = "4412705";
    public static final String BANK_CODE = "4410010";
    public static final String TERMINAL_ID = "TMC0000000000001";
    public static final String SWITCHER_ID = "0000000";

    public static final String SIGN_ON = "001";
    public static final String SIGN_OFF = "002";
    public static final String ECHO_TEST = "003";
    private static final Random rand = new Random();

    public static String date14() {
        return new SimpleDateFormat("YYYYMMDDhhmm").format(new Date()) +""+new SimpleDateFormat("ss").format(new Date()).substring(0,1);
    }

    public static String date8()
    {
        return new SimpleDateFormat("ddhhmmss").format(new Date());
    }


    /**
     * STAN NUMBER GENERATOR
     * @return STRING
     */

    public static synchronized String Stan(){
        String psqlAutoincrementQuery = "select nextval('stan')";
        return String.valueOf(SessionUtils.getSession().createSQLQuery(psqlAutoincrementQuery).getSingleResult());
    }

    public static String padLeftWithZero(String value,int length){
        return StringUtils.leftPad(value,length,"0");
    }
    
    public static String numberToPay(double number){
        int a = (int) number;
        return String.valueOf(a);
    }

    public static String amountToPay(double amount) {
        String curency = "360";
        String minor = "";
        String harga ="";
        if(amount % 1 == 0){
            minor = "0";
            harga = String.valueOf(Integer.toString((int)amount));
        }else{
            minor = "2";
            harga = String.valueOf(Integer.toString((int)(amount * 100)));
        }
        return curency + minor + StringUtils.leftPad(harga,12,"0");
    }

    public static String valueToMinor(double amount,int minor) {
        if(amount % 1 == 0){
            StringBuilder s = new StringBuilder();
            for(int i=1;i<= minor;i++){
                s.append("0");
            }

            return new DecimalFormat("#").format(amount) + s.toString();
        }else{
            StringBuilder s = new StringBuilder();
            s.append("1");
            for(int i=1;i<= minor;i++) {
                s.append("0");
            }
            return new DecimalFormat("#").format(amount * Integer.parseInt(s.toString()));
        }
    }

    public double numberMinorUnit(String val, int min) {
        int bagi = Integer.parseInt("1" + StringUtils.leftPad("", min, "0"));
        return Double.parseDouble(val) / bagi;
    }
}
