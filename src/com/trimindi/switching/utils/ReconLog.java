package com.trimindi.switching.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by HP on 31/05/2017.
 */
public class ReconLog {
    private String PATH = "recon/";
    private File logFile;
    private String date;
    private String PARTNER_ID;
    private String PRODUCT_CODE;

    public ReconLog(String PARTNER_ID,String PRODUCT_CODE) {
        this.PARTNER_ID = PARTNER_ID;
        this.PRODUCT_CODE = PRODUCT_CODE;

    }
    public void writeLog(String DISTRIBUTION_CODE,String s) {
        try {
            String Date = new SimpleDateFormat("yyyyMMdd").format(new Date());
            String filename = PATH + PARTNER_ID +"-"+PRODUCT_CODE+"-"+DISTRIBUTION_CODE+"-"+Date+".ftr";
            logFile = new File(filename);
            FileWriter fw = new FileWriter(this.logFile,true);
            fw.write(s);
            fw.write(System.lineSeparator());
            fw.close();
        } catch (IOException ex) {
            System.err.println("Couldn't log this: "+s);
        }
    }
}
