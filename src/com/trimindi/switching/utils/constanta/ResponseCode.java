package com.trimindi.switching.utils.constanta;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.ws.Response;

/**
 * Created by sx on 13/05/17.
 */
@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseCode {
    public static final ResponseCode SERVER_UNAVAILABLE = new ResponseCode("9999","TERJADI KESALAHAN PADA SERVER");
    public static final ResponseCode SERVER_TIMEOUT = new ResponseCode("9998", "WAKTU REQUEST MELEIBIHI BATAS");
    public static final ResponseCode PARAMETER_SALDO = new ResponseCode("1003","SALDO ANDA TIDAK MENCUKUPI");
    public static final ResponseCode PARAMETER_UNKNOW_DENOM = new ResponseCode("1002","KODE PRODUCT TIDAK DI KENALI");
    public static final ResponseCode NTRANS_NOT_FOUND = new ResponseCode("1006","NTRANS TIDAK TERDAFTAR");
    public static final ResponseCode MISSING_REQUIRED_PARAMETER = new ResponseCode("8999","PARAMETER REQUEST MISSING");
    public static final ResponseCode PAYMENT_UNDER_PROSES = new ResponseCode("1007","PEMBAYARAN SEDANG DALAM PROSES TUNGGU BEBERAPA SAAT LAGI");
    public static final ResponseCode PAYMENT_DONE = new ResponseCode("1008","NTRANS SUDAH TIDAK BERLAKU PEMBAYARAN BERHASIL");
    public static final ResponseCode PAYMENT_FAILED = new ResponseCode("1009","PEMBAYARAN / PEMBELIAN GAGAL.");
    public static final ResponseCode MAXIMUM_PRINTED_RECIPT = new ResponseCode("1010", "MELEBIHI BATAS MAKSIMUM CETAK ULANG");
    public static final ResponseCode UNKNOW_ACTION = new ResponseCode("2000", "ACTION TIDAK DI KENALI");
    public static final ResponseCode INVALID_BODY_REQUEST_FORMAT = new ResponseCode("2001","REQUEST BODY TIDAK VALID");
    public static final ResponseCode PRODUCT_UNDER_MAINTANCE = new ResponseCode("2002", "PRODUCT MASIH DALAM MAINTANCE");
    public static final ResponseCode PERMISION_USER_DENIED = new ResponseCode("2011", "PARTNER TIDAK MEMILIKI AKSES ACTION");
    public static final ResponseCode PARAMETER_INVALID_PRODUCT_ACTION = new ResponseCode("2012", "PRODUCT DAN ACTION TIDAK VALID");
    private boolean status;
    private String code;
    private String message;
    private String ntrans;
    private String product;
    public ResponseCode() {
    }

    public String getProduct() {
        return product;
    }

    public ResponseCode setProduct(String product) {
        this.product = product;
        return this;
    }

    @XmlElement(name = "ntrans")
    public String getNtrans() {
        return ntrans;
    }

    public ResponseCode setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public ResponseCode(String code, String message) {
        this.code = code;
        this.status = false;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public ResponseCode setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public String getCode() {
        return code;
    }

    public ResponseCode setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseCode setMessage(String message) {
        this.message = message;
        return this;
    }
}
