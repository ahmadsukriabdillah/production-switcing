package com.trimindi.switching.utils;

import com.trimindi.switching.response.prepaid.Payment;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPrePaid;
import com.trimindi.switching.packager.PLNPackager;
import com.trimindi.switching.utils.iso.builder.FieldBuilder;
import com.trimindi.switching.utils.iso.parsing.SDE;
import org.apache.commons.lang3.StringUtils;
import org.jpos.iso.ISOMsg;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by HP on 31/05/2017.
 */
public class ReconGenerator {

    public static String generatePrepaid(ISOMsg response){
        try {
            ISOMsg msg = (ISOMsg) response.clone();
            msg.setPackager(new PLNPackager());
            List<Rules> p = new SDE.Builder()
                    .setPayload(msg.getString(48))
                    .setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(48,true))
                    .generate();
            List<Rules> d = new SDE.Builder()
                    .setPayload(msg.getString(62))
                    .setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(62,true))
                    .generate();
            p.addAll(d);
            p.add(new Rules(msg.getString(63)));
            Payment payment = new Payment(p,true);
            String amout = msg.getString(4);
            int minor = Integer.parseInt(amout.substring(3,4));
            String TAGIHAN = amout.substring(4,16);
            return new FieldBuilder.Builder()
                    .addValue(msg.getString(12),12,"0","L")
                    .addValue("|",1,"|","L")
                    .addValue(msg.getString(33),7,"0","L")
                    .addValue("|",1,"|","L")
                    .addValue(msg.getString(26),4,"0","L")
                    .addValue("|",1,"|","L")
                    .addValue(payment.getPLNReferenceNumber(),32," ","R")
                    .addValue("|",1,"|","L")
                    .addValue(payment.getBukopinReferenceNumber(),32," ","R")
                    .addValue("|",1,"|","L")
                    .addValue(payment.getMeterSerialNumber(),11," ","R")
                    .addValue("|",1,"|","L")
                    .addValue(TAGIHAN + addZeroBack(minor),10,"0","L")
                    .addValue("|",1,"|","L")
                    .addValue(new DecimalFormat("#").format(payment.getAdmin()) + "00",10,"0","L")
                    .addValue("|",1,"|","L")
                    .addValue(new DecimalFormat("#").format(payment.getStampDuty())+ "00",10,"0","L")
                    .addValue("|",1,"|","L")
                    .addValue(new DecimalFormat("#").format(payment.getValueAddedTax())+"00",10,"0","L")
                    .addValue("|",1,"|","L")
                    .addValue(new DecimalFormat("#").format(payment.getPublicLightingTax())+"00",10,"0","L")
                    .addValue("|",1,"|","L")
                    .addValue(new DecimalFormat("#").format(payment.getCustomerPayablesInstallment())+"00",10,"0","L")
                    .addValue("|",1,"|","L")
                    .addValue(new DecimalFormat("#").format(payment.getPowerPurchase())+"00",12,"0","L")
                    .addValue("|",1,"|","L")
                    .addValue(new DecimalFormat("#").format(payment.getPurchasedKWHUnit())+"00",10,"0","L")
                    .addValue("|",1,"|","L")
                    .addValue(payment.getTokenNumber(),20," ","L")
                    .addValue("|",1,"|","L")
                    .addValue(msg.getString(32),7," ","L")
                    .addValue("|",1,"|","L")
                    .addValue(msg.getString(41),16," ","L")
                    .build();
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

//    public static String generatePostpaid(ISOMsg isoMsg){
//        ISOMsg msg = (ISOMsg) isoMsg.clone();
//        List<Rules> res = new SDE.Builder()
//                .setPayload(msg.getString(48))
//                .setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, true))
//                .generate();
//        res.add(new Rules(msg.getString(63)));
//        com.trimindi.switching.response.postpaid.PaymentPDAM paymentResponse = new com.trimindi.switching.response.postpaid.PaymentPDAM(res);
//        String amout = msg.getString(4);
//        int minor = Integer.parseInt(amout.substring(3,4));
//        String TAGIHAN = amout.substring(4,16);
//        return new FieldBuilder.Builder()
//                .addValue(msg.getString(12),12,"0","L")
//                .addValue("|",1,"|","L")
//                .addValue(msg.getString(32),7,"0","L")
//                .addValue("|",1,"|","L")
//                .addValue(msg.getString(33),7,"0","L")
//                .addValue("|",1,"|","L")
//                .addValue(msg.getString(26),4,"0","L")
//                .addValue("|",1,"|","L")
//                .addValue(paymentResponse.getBukopinReferenceNumber(),32," ","R")
//                .addValue("|",1,"|","L")
//                .addValue(paymentResponse.getSubscriberID(),12," ","L")
//                .addValue("|",1,"|","L")
//                .addValue(paymentResponse.getBillPeriod(),6," ","L")
//                .addValue("|",1,"|","L")
//                .addValue(TAGIHAN + addZeroBack(minor),15,"0","L")
//                .addValue("|",1,"|","L")
//                .addValue(new DecimalFormat("#").format(paymentResponse.getTotalElectricityBill()),15,"0","L")
//                .addValue("|",1,"|","L")
//                .addValue(new DecimalFormat("#").format(paymentResponse.getTotalOutstandingBill()),15,"0","L")
//                .addValue("|",1,"|","L")
//                .addValue(paymentResponse.getIncentive(),11,"0","L")
//                .addValue("|",1,"|","L")
//                .addValue(new DecimalFormat("#").format(paymentResponse.getValueAddedTax()),10,"0","L")
//                .addValue("|",1,"|","L")
//                .addValue(new DecimalFormat("#").format(paymentResponse.getPenaltyFee()),12,"0","L")
//                .addValue("|",1,"|","L")
//                .addValue(new DecimalFormat("#").format(paymentResponse.getADMIN()),9,"0","L")
//                .addValue("|",1,"|","L")
//                .addValue(msg.getString(41),16," ","L")
//                .build();
//    }

    private static String addZeroBack(int num){
        return StringUtils.leftPad("",num,"0");
    }
}
