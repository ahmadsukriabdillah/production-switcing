package com.trimindi.switching.thread.prepaid;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.client.ChannelManager;
import com.trimindi.switching.controllers.Request;
import com.trimindi.switching.models.PartnerDeposit;
import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.response.prepaid.Payment;
import com.trimindi.switching.response.prepaid.PaymentResponse;
import com.trimindi.switching.services.PartnerDepositService;
import com.trimindi.switching.services.TransaksiService;
import com.trimindi.switching.thread.BaseThread;
import com.trimindi.switching.utils.ReconGenerator;
import com.trimindi.switching.utils.ReconLog;
import com.trimindi.switching.utils.TLog;
import com.trimindi.switching.utils.constanta.ResponseCode;
import com.trimindi.switching.utils.constanta.TStatus;
import com.trimindi.switching.utils.generator.BaseHelper;
import com.trimindi.switching.utils.generator.PrePaidGenerator;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPrePaid;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.NameRegistrar;

import javax.net.ssl.SSLContext;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by HP on 15/05/2017.
 */
public class PrepaidPayment extends BaseThread implements Runnable {
    private static final String LOG = PrepaidPayment.class.getSimpleName();
    private static HttpPost httpPost;
    private Transaksi transaksi;
    private AsyncResponse response;
    private ChannelManager channelManager;
    private TransaksiService transaksiService;
    private PartnerDepositService partnerDepositService;
    private List<Rules> rule;
    private ISOMsg payment = null;
    private ReconLog reconLog;
    private ObjectMapper objectMapper;
    private CloseableHttpClient client;

    public PrepaidPayment(AsyncResponse asyncResponse, Transaksi t, Request req) {
        super();
        try {
            this.reconLog = new ReconLog(BaseHelper.PARTNER_ID,BaseHelper.PAN_PREPAID);
            this.transaksi = t;
            this.partnerDepositService = new PartnerDepositService();
            this.transaksiService = new TransaksiService();
            this.response = asyncResponse;
            this.channelManager = (ChannelManager) NameRegistrar.get("manager");
            response.setTimeoutHandler(new TimeoutHandler() {
                @Override
                public void handleTimeout(AsyncResponse asyncResponse) {
                    sendBack(ResponseCode.PAYMENT_UNDER_PROSES.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                }
            });
            response.setTimeout(60, TimeUnit.SECONDS);
            this.objectMapper = new ObjectMapper();
            SSLContext sslContext = null;
            try {
                sslContext = new SSLContextBuilder()
                        .loadTrustMaterial(null, new TrustStrategy() {
                            @Override
                            public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                                return true;
                            }
                        }).build();
                client = HttpClients.custom()
                        .setSSLContext(sslContext)
                        .setSSLHostnameVerifier(new NoopHostnameVerifier())
                        .build();
                httpPost = new HttpPost(req.BACK_LINK);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }
        } catch (NameRegistrar.NotFoundException e) {
            sendBack(ResponseCode.SERVER_UNAVAILABLE.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
        }
    }

    @Override
    public void run() {
        super.run();
        try {
            payment = channelManager.sendMsg(PrePaidGenerator.generatePurchase(transaksi));
            if (payment != null) {
                if(payment.getString(39).equals("0000")){
                    PaymentResponse paymentResponse = transactionSuccess(payment);
                    sendBack(
                            Response.status(200)
                                    .entity(paymentResponse)
                                    .build()
                    );
                }else{
                    /**
                     * 0005
                     * 0063
                     * 0068
                     */
                    if(failedCode.containsKey(payment.getString(39))) {
                        ISOMsg advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdvice(transaksi));
                        if(advice != null){
                            if(!advice.getString(39).equals("0000")){
                                advice = null;
                                rule = parsingRules(advice,false);
                                advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdviceRepeat(transaksi));
                                if(advice != null){
                                    if(!advice.getString(39).equals("0000")){
                                        rule = parsingRules(advice,false);
                                        partnerDepositService.reverse(transaksi, advice);
                                        sendBack(
                                                Response.status(200)
                                                        .entity(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()))
                                                        .build()
                                        );
                                    }else{
                                        PaymentResponse paymentResponse = transactionSuccess(advice);
                                        sendBack(
                                                Response.status(200)
                                                        .entity(paymentResponse)
                                                        .build()
                                        );

                                    }
                                }else{
                                    partnerDepositService.reverse(transaksi,advice);
                                }
                            }else{
                                PaymentResponse paymentResponse = transactionSuccess(advice);
                                sendBack(
                                        Response.status(200)
                                                .entity(paymentResponse)
                                                .build()
                                );
                            }
                        }else{
                            advice = null;
                            advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdviceRepeat(transaksi));
                            if(advice != null) {
                                if (!advice.getString(39).equals("0000")) {
                                    rule = parsingRules(advice, false);
                                    partnerDepositService.reverse(transaksi, advice);
                                    sendBack(
                                            Response.status(200)
                                                    .entity(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()))
                                                    .build()
                                    );
                                } else {
                                    PaymentResponse paymentResponse = transactionSuccess(advice);
                                    sendBack(
                                            Response.status(200)
                                                    .entity(paymentResponse)
                                                    .build()
                                    );
                                }
                            }
                        }

                    }else{
                        partnerDepositService.reverse(transaksi, payment);
                        sendBack(Response.status(200).entity(responseCode.get(payment.getString(39)).setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM())).build());
                    }
                }
            }else{
                ISOMsg advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdvice(transaksi));
                if(advice != null){
                    if(!advice.getString(39).equals("0000")){
                        if(failedCode.containsKey(advice.getString(39)))
                        {
                            advice = null;
                            rule = parsingRules(advice,false);
                            advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdviceRepeat(transaksi));
                            if(advice != null){
                                if(!advice.getString(39).equals("0000")){
                                    rule = parsingRules(advice,false);
                                    partnerDepositService.reverse(transaksi, advice);
                                    sendBack(
                                            Response.status(200)
                                                    .entity(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()))
                                                    .build()
                                    );
                                }else{
                                    PaymentResponse paymentResponse = transactionSuccess(advice);
                                    sendBack(
                                            Response.status(200)
                                                    .entity(paymentResponse)
                                                    .build()
                                    );
                                }
                            }else{
                                partnerDepositService.reverse(transaksi,advice);
                            }
                        }else{
                            partnerDepositService.reverse(transaksi, advice);
                            sendBack(Response.status(400).entity(responseCode.get(advice.getString(39)).setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM())).build());
                        }
                    }else{
                        PaymentResponse paymentResponse = transactionSuccess(advice);
                        sendBack(
                                Response.status(200)
                                        .entity(paymentResponse)
                                        .build()
                        );
                    }
                }else{
                    advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdviceRepeat(transaksi));
                    if(advice != null){
                        if(advice.getString(39).equals("0000")){
                            PaymentResponse paymentResponse = transactionSuccess(advice);
                            sendBack(
                                    Response.status(200)
                                            .entity(paymentResponse)
                                            .build()
                            );
                        }else if(failedCode.containsKey(advice.getString(39))){
                            partnerDepositService.reverse(transaksi,advice);
                        }else{
                            partnerDepositService.reverse(transaksi,advice);

                        }
                    }else{
                        advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdviceRepeat(transaksi));
                        if(advice != null){
                            if(advice.getString(39).equals("0000")){
                                PaymentResponse paymentResponse = transactionSuccess(advice);
                                sendBack(
                                        Response.status(200)
                                                .entity(paymentResponse)
                                                .build()
                                );
                            }else{
                                partnerDepositService.reverse(transaksi,advice);
                            }
                        }else{
                            partnerDepositService.reverse(transaksi,advice);

                        }
                    }
                }
            }
        } catch (ISOException e) {
            TLog.log(LOG + " - " + e.getMessage());
            try {
                partnerDepositService.reverse(transaksi, payment);
            } catch (ISOException e1) {
                TLog.log(LOG + " - " + e.getMessage());
            }
            sendBack(ResponseCode.SERVER_UNAVAILABLE.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
        } catch (InterruptedException e) {
            TLog.log(LOG + " - " + e.getMessage());
            try {
                partnerDepositService.reverse(transaksi, payment);
            } catch (ISOException e1) {
                TLog.log(LOG + " - " + e.getMessage());
            }
            sendBack(ResponseCode.SERVER_UNAVAILABLE.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
        } catch (Exception e) {
            TLog.log(LOG + " - " + e.getMessage());
            try {
                partnerDepositService.reverse(transaksi, payment);
            } catch (ISOException e1) {
                TLog.log(LOG + " - " + e.getMessage());
            }
            sendBack(ResponseCode.SERVER_UNAVAILABLE.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
        }

    }

    private PaymentResponse transactionSuccess(ISOMsg msg) {
        try {
            rule = parsingRules(msg,true);
            Payment payment = new Payment(rule,true);
            transaksi.setTIME_PAYMENT(new Timestamp(System.currentTimeMillis())).setPAYMENT(new String(this.payment.pack()))
                    .setST(TStatus.PAYMENT_SUCCESS)
                    .setDATA(payment.getTokenNumber())
                    .setBILL_REF_NUMBER(payment.getBukopinReferenceNumber());
            transaksiService.update(transaksi);
            PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(transaksi.getPARTNERID());
            PaymentResponse baseResponse = new PaymentResponse();
            baseResponse.setData(payment);
            baseResponse.setNtrans(transaksi.getNTRANS());
            baseResponse.setFee(transaksi.getFEE());
            baseResponse.setSaldo(partnerDeposit.getBALANCE());
            baseResponse.setTagihan(transaksi.getAMOUT());
            baseResponse.setTotalBayar(transaksi.getCHARGE());
            baseResponse.setTotalFee(transaksiService.getTotalFee(transaksi.getPARTNERID()));
            baseResponse.setProduct(transaksi.getDENOM());
            return baseResponse;
        } catch (ISOException e) {
            TLog.log(LOG + " - " + e.getMessage());
        }
        return null;
    }

    private void sendBack(ResponseCode r){
        if(response.isSuspended()){
            response.resume(Response.status(200).entity(r).build());
        }
    }
    private void sendBack(Response r){
        if(response.isSuspended()){
            response.resume(r);
        } else {
            try {
                StringEntity entity = new StringEntity(objectMapper.writeValueAsString(r.getEntity()));
                httpPost.setEntity(entity);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                CloseableHttpResponse response = null;
                response = client.execute(httpPost);
                String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
                System.out.println(respone);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private List<Rules> parsingRules(ISOMsg d,boolean status) {
        try{
                List<Rules> bit48 = new SDE.Builder()
                        .setPayload(d.getString(48))
                        .setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(48,status))
                        .generate();
                List<Rules> bit62 = new SDE.Builder()
                        .setPayload(d.getString(62))
                        .setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(62,status))
                        .generate();
                bit48.addAll(bit62);
                bit48.add(new Rules(d.getString(63)));
                return bit48;
            }catch (Exception e){
                TLog.log(LOG + " - " + e.getMessage());
        }
        return null;
    }
}
