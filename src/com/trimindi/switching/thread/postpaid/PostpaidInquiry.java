package com.trimindi.switching.thread.postpaid;

import com.trimindi.switching.client.ChannelManager;
import com.trimindi.switching.models.*;
import com.trimindi.switching.response.postpaid.Inquiry;
import com.trimindi.switching.response.postpaid.InquiryResponse;
import com.trimindi.switching.response.postpaid.Rincian;
import com.trimindi.switching.services.PartnerDepositService;
import com.trimindi.switching.services.ProductFeeService;
import com.trimindi.switching.services.TransaksiService;
import com.trimindi.switching.thread.BaseThread;
import com.trimindi.switching.utils.PostpaidHelper;
import com.trimindi.switching.utils.TLog;
import com.trimindi.switching.utils.constanta.Constanta;
import com.trimindi.switching.utils.constanta.ResponseCode;
import com.trimindi.switching.utils.constanta.TStatus;
import com.trimindi.switching.utils.generator.PostPaidGenerator;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.NameRegistrar;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by HP on 15/05/2017.
 */
public class PostpaidInquiry extends BaseThread implements Runnable {
    private static final String LOG = PostpaidInquiry.class.getSimpleName();
    private PartnerCredential partnerCredential;
    private Map<String, String> params;
    private AsyncResponse response;
    private ChannelManager channelManager;
    private PartnerDepositService partnerDepositService;
    private TransaksiService transaksiService;
    private ProductFeeService productFeeService;
    private ProductItem productItem;


    public PostpaidInquiry(AsyncResponse asyncResponse, Map<String, String> request, PartnerCredential partnerCredential, final ProductItem productItem) {
        super();
        try {
            this.productItem = productItem;
            this.productFeeService = new ProductFeeService();
            this.partnerDepositService = new PartnerDepositService();
            this.transaksiService = new TransaksiService();
            this.partnerCredential = partnerCredential;
            this.params = request;
            this.response = asyncResponse;
            this.channelManager = (ChannelManager) NameRegistrar.get("manager");
            response.setTimeoutHandler(new TimeoutHandler() {
                @Override
                public void handleTimeout(AsyncResponse asyncResponse) {
                    sendBack(ResponseCode.SERVER_TIMEOUT.setProduct(productItem.getDENOM()));
                }
            });
            response.setTimeout(40, TimeUnit.SECONDS);
        } catch (NameRegistrar.NotFoundException e) {
            sendBack(ResponseCode.SERVER_UNAVAILABLE.setProduct(productItem.getDENOM()));
        }
    }

    @Override
    public void run() {
        super.run();
        ISOMsg inquiry;
        try {
            inquiry = channelManager.sendMsg(PostPaidGenerator.generateInquiry(params.get(Constanta.MACHINE_TYPE),params.get(Constanta.MSSIDN)));
            if (inquiry != null) {
                if(inquiry.getString(39).equals("0000")){
                    Transaksi transaksi = new Transaksi();
                    boolean status = true;
                    String inquiryRes = inquiry.getString(48);
                    List<Rules> bit48 = new SDE.Builder()
                            .setPayload(inquiryRes)
                            .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48,status))
                            .generate();
                    int legtht = new SDE.Builder()
                            .setPayload(inquiryRes)
                            .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48,status))
                            .calculate();

                    String rincian = inquiryRes.substring(legtht,inquiryRes.length());
                    Inquiry inquiryResponse = new Inquiry(bit48,true);
                    List<Rincian> rincians = new ArrayList<>();
                    int start = 0;
                    int leghtRincian = 115;
                    double total = 0;
                    double denda = 0;
                    for(int i= 0;i<inquiryResponse.getBillStatus();i++){
                        String parsRincian = rincian.substring(start,start+leghtRincian);
                        List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
                        Rincian r = new Rincian(rc);
                        rincians.add(r);
                        total += r.getTotalElectricityBill();
                        denda += r.getPenaltyFee();
                        start += leghtRincian;
                    }
                    total += denda;
                    inquiryResponse.setRincian(rincians);
                    /**
                     * MT = MACHINE_TYPE
                     * PC = PRODUDUCT CODE
                     * PRODUCT = PRODUCT
                     */
                    ProductFee productFee = productFeeService.findFee(partnerCredential.getPartner_id(),params.get(Constanta.DENOM));
                    PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(partnerCredential.getPartner_id());

                    transaksi
                            .setADMIN(inquiryResponse.getAdmin())
                            .setMSSIDN_NAME(inquiryResponse.getSubscriberName())
                            .setBILL_REF_NUMBER(inquiryResponse.getBukopinTbkReferenceNumber())
                            .setMSSIDN(inquiryResponse.getSubscriberID())
                            .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                            .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                            .setPRODUCT(params.get(Constanta.PRODUCT_CODE))
                            .setAMOUT(total)
                            .setDENOM(productItem.getDENOM())
                            .setPRODUCT(productItem.getProduct_id())
                            .setFEE(productFee.getFEE() * inquiryResponse.getBillStatus())
                            .setUSERID(partnerCredential.getPartner_uid())
                            .setINQUIRY(new String(inquiry.pack()))
                            .setCHARGE(total + inquiryResponse.getAdmin() - (productFee.getFEE() * inquiryResponse.getBillStatus()))
                            .setDEBET(total + inquiryResponse.getAdmin() - (productFee.getFEE() * inquiryResponse.getBillStatus()))
                            .setMAC_ADDRESS(params.get(Constanta.MAC))
                            .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                            .setPARTNERID(partnerCredential.getPartner_id())
                            .setLATITUDE(params.get(Constanta.LATITUDE))
                            .setLONGITUDE(params.get(Constanta.LONGITUDE))
                            .setPINALTY(denda)
                            .setST(TStatus.INQUIRY);
                    transaksiService.persist(transaksi);
                    inquiryResponse.setPeriod(PostpaidHelper.generatePeriode(rincians));
                    inquiryResponse.setMeter(PostpaidHelper.generateStandMeter(rincians));
                    inquiryResponse.setTagihan(transaksi.getAMOUT());
                    InquiryResponse baseResponse = new InquiryResponse();
                    baseResponse.setData(inquiryResponse);
                    baseResponse.setFee(transaksi.getFEE()).setNtrans(transaksi.getNTRANS());
                    baseResponse.setSaldo(partnerDeposit.getBALANCE());
                    baseResponse.setTotalFee(transaksiService.getTotalFee(partnerCredential.getPartner_id()));
                    baseResponse.setTagihan(transaksi.getAMOUT());
                    baseResponse.setTotalBayar(transaksi.getCHARGE());
                    baseResponse.setProduct(transaksi.getDENOM());
                    sendBack(
                            Response.status(200)
                                    .entity(baseResponse)
                                    .build()
                    );
                }else{
                    sendBack(
                            Response.status(200)
                                    .entity(responseCode.get(inquiry.getString(39)).setProduct(productItem.getDENOM()))
                                    .build()
                    );
                }
            }
        } catch (ISOException e) {
            TLog.log(LOG + " - " + e.getMessage());
            sendBack(ResponseCode.SERVER_UNAVAILABLE.setProduct(productItem.getDENOM()));
        } catch (Exception e) {
            TLog.log(LOG + " - " + e.getMessage());
            sendBack(ResponseCode.SERVER_UNAVAILABLE.setProduct(productItem.getDENOM()));
        }

    }
    private void sendBack(ResponseCode r){
        if(response.isSuspended()){
            response.resume(Response.status(200).entity(r).build());
        }
    }
    private void sendBack(Response r){
        if(response.isSuspended()) {
            response.resume(r);
        }
    }
}
