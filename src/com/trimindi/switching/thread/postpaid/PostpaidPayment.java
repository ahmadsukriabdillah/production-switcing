package com.trimindi.switching.thread.postpaid;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.client.ChannelManager;
import com.trimindi.switching.controllers.Request;
import com.trimindi.switching.models.PartnerDeposit;
import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.response.postpaid.Payment;
import com.trimindi.switching.response.postpaid.PaymentResponse;
import com.trimindi.switching.response.postpaid.Rincian;
import com.trimindi.switching.services.PartnerDepositService;
import com.trimindi.switching.services.TransaksiService;
import com.trimindi.switching.thread.BaseThread;
import com.trimindi.switching.utils.PostpaidHelper;
import com.trimindi.switching.utils.ReconLog;
import com.trimindi.switching.utils.TLog;
import com.trimindi.switching.utils.constanta.ResponseCode;
import com.trimindi.switching.utils.constanta.TStatus;
import com.trimindi.switching.utils.generator.BaseHelper;
import com.trimindi.switching.utils.generator.PostPaidGenerator;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.NameRegistrar;

import javax.net.ssl.SSLContext;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by HP on 15/05/2017.
 */
public class PostpaidPayment extends BaseThread implements Runnable {
    private static final String LOG = PostpaidPayment.class.getSimpleName();
    private static HttpPost httpPost;
    private AsyncResponse response;
    private ChannelManager channelManager;
    private Transaksi transaksi;
    private TransaksiService transaksiService;
    private PartnerDepositService partnerDepositService;
    private List<Rules> rule;
    private ISOMsg payment = null;
    private ReconLog reconLog;
    private int legth;
    private String rincian;
    private ObjectMapper objectMapper;
    private CloseableHttpClient client;


    public PostpaidPayment(AsyncResponse asyncResponse, final Transaksi transaksi, Request req) {
        try {
            this.reconLog = new ReconLog(BaseHelper.PARTNER_ID,BaseHelper.PAN_POSTPAID);
            this.transaksi = transaksi;
            this.partnerDepositService = new PartnerDepositService();
            this.transaksiService = new TransaksiService();
            this.response = asyncResponse;
            this.channelManager = (ChannelManager) NameRegistrar.get("manager");
            response.setTimeoutHandler(new TimeoutHandler() {
                @Override
                public void handleTimeout(AsyncResponse asyncResponse) {
                    sendBack(ResponseCode.PAYMENT_UNDER_PROSES.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                }
            });
            response.setTimeout(60, TimeUnit.SECONDS);
            this.objectMapper = new ObjectMapper();
            SSLContext sslContext = null;
            try {
                sslContext = new SSLContextBuilder()
                        .loadTrustMaterial(null, new TrustStrategy() {
                            @Override
                            public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                                return true;
                            }
                        }).build();
                client = HttpClients.custom()
                        .setSSLContext(sslContext)
                        .setSSLHostnameVerifier(new NoopHostnameVerifier())
                        .build();
                httpPost = new HttpPost(req.BACK_LINK);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }
        } catch (NameRegistrar.NotFoundException e) {
            sendBack(ResponseCode.SERVER_UNAVAILABLE.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
        }
    }

    @Override
    public void run() {
        super.run();
        try {
            ISOMsg paymentRequest = PostPaidGenerator.generatePurchase(transaksi);
            payment = channelManager.sendMsg(paymentRequest);
            if (payment != null) {
                if(payment.getString(39).equals("0000")){
                    boolean status = true;
                    rule = parsingRules(this.payment, status);
                    Payment payment = new Payment(rule);
                    transaksi.setPAYMENT(new String(this.payment.pack())).setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()))
                            .setST(TStatus.PAYMENT_SUCCESS)
                            .setBILL_REF_NUMBER(payment.getBukopinReferenceNumber());

                    transaksiService.update(transaksi);
                    int start = 0;
                    int leghtRincian = 115;
                    List<Rincian> rincians = new ArrayList<>();
                    for(int i = 0; i< payment.getBillStatus(); i++){
                        String parsRincian = rincian.substring(start,start+leghtRincian);
                        List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
                        Rincian r = new Rincian(rc);
                        rincians.add(r);
                        start += leghtRincian;
                    }
                    payment.setRincians(rincians);
                    payment.setMeter(PostpaidHelper.generateStandMeter(rincians));
                    payment.setPeriod(PostpaidHelper.generatePeriode(rincians));
                    PaymentResponse baseResponse = new PaymentResponse();
                    PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(transaksi.getPARTNERID());
                    baseResponse.setData(payment);
                    baseResponse.setFee(transaksi.getFEE());
                    baseResponse.setNtrans(transaksi.getNTRANS());
                    baseResponse.setTagihan(transaksi.getAMOUT());
                    baseResponse.setTotalBayar(transaksi.getCHARGE());
                    baseResponse.setSaldo(partnerDeposit.getBALANCE());
                    baseResponse.setTotalFee(transaksiService.getTotalFee(transaksi.getPARTNERID()));
                    baseResponse.setProduct(transaksi.getDENOM());
                    sendBack(
                            Response.status(200)
                                    .entity(baseResponse)
                                    .build()
                    );
                }else{
                    if (failedCode.containsKey(payment.getString(39))) {
                        payment = null;
                        ISOMsg reversal = PostPaidGenerator.generateReversal(paymentRequest, "2400");
                        payment = channelManager.sendMsg(reversal);
                        if (payment != null) {
                            if (payment.getString(39).equals("0000")) {
                                partnerDepositService.reverse(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                            } else if (failedCode.containsKey(payment.getString(39))) {
                                reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                                payment = null;
                                payment = channelManager.sendMsg(reversal);
                                if (payment != null) {
                                    if (payment.getString(39).equals("0000")) {
                                        partnerDepositService.reverse(transaksi, payment);
                                        sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                    } else if(failedCode.containsKey(payment.getString(39))) {
                                        reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                                        payment = null;
                                        payment = channelManager.sendMsg(reversal);
                                        if (payment != null){
                                            if(payment.getString(39).equals("0000")){
                                                partnerDepositService.reverse(transaksi, payment);
                                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                            }else{
                                                partnerDepositService.reverse(transaksi, payment);
                                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                            }
                                        }else{
                                            partnerDepositService.reverse(transaksi, payment);
                                            sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                        }

                                    }else{
                                        partnerDepositService.reverse(transaksi, payment);
                                        sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                    }
                                }else{
                                    partnerDepositService.reverse(transaksi, payment);
                                    sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                }
                            } else {
                                partnerDepositService.reverse(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                            }
                        }else{
                            partnerDepositService.reverse(transaksi, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                        }

                    } else {
                        partnerDepositService.reverse(transaksi, payment);
                        sendBack(
                                Response.status(200)
                                        .entity(responseCode.get(payment.getString(39)).setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()))
                                        .build()
                        );
                    }
                }
            } else {
                payment = null;
                ISOMsg reversal = PostPaidGenerator.generateReversal(paymentRequest, "2400");
                payment = channelManager.sendMsg(reversal);
                if (payment != null) {
                    if (payment.getString(39).equals("0000")) {
                        partnerDepositService.reverse(transaksi, payment);
                        sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                    } else if (failedCode.containsKey(payment.getString(39))) {
                        reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                        payment = null;
                        payment = channelManager.sendMsg(reversal);
                        if (payment != null) {
                            if (payment.getString(39).equals("0000")) {
                                partnerDepositService.reverse(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                            } else if(failedCode.containsKey(payment.getString(39))) {
                                reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                                payment = null;
                                payment = channelManager.sendMsg(reversal);
                                if (payment != null){
                                    if(payment.getString(39).equals("0000")){
                                        partnerDepositService.reverse(transaksi, payment);
                                        sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                    }else{
                                        partnerDepositService.reverse(transaksi, payment);
                                        sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                    }
                                }else{
                                    partnerDepositService.reverse(transaksi, payment);
                                    sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                }
                            } else {
                                partnerDepositService.reverse(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                            }
                        }else{
                            reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                            payment = null;
                            payment = channelManager.sendMsg(reversal);
                            if (payment != null){
                                if(payment.getString(39).equals("0000")){
                                    partnerDepositService.reverse(transaksi, payment);
                                    sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                }else{
                                    partnerDepositService.reverse(transaksi, payment);
                                    sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                }
                            }else{
                                partnerDepositService.reverse(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                            }
                        }
                    } else {
                        partnerDepositService.reverse(transaksi, payment);
                        sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                    }
                }else{
                    reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                    payment = null;
                    payment = channelManager.sendMsg(reversal);
                    if (payment != null) {
                        if (payment.getString(39).equals("0000")) {
                            partnerDepositService.reverse(transaksi, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                        } else if (failedCode.containsKey(payment.getString(39))) {
                            reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                            payment = null;
                            payment = channelManager.sendMsg(reversal);
                            if (payment != null){
                                if(payment.getString(39).equals("0000")){
                                    partnerDepositService.reverse(transaksi, payment);
                                    sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                }else{
                                    partnerDepositService.reverse(transaksi, payment);
                                    sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                                }
                            }else{
                                partnerDepositService.reverse(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                            }
                        } else {
                            partnerDepositService.reverse(transaksi, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                        }
                    }else{
                        reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                        payment = null;
                        payment = channelManager.sendMsg(reversal);
                        if (payment != null){
                            if(payment.getString(39).equals("0000")){
                                partnerDepositService.reverse(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                            }else{
                                partnerDepositService.reverse(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                            }
                        }else{
                            reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                            payment = null;
                            payment = channelManager.sendMsg(reversal);
                            partnerDepositService.reverse(transaksi, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
                        }
                    }
                }
            }
        } catch (Exception e) {
            TLog.log(LOG + " - " + e.getMessage());
            try {
                partnerDepositService.reverse(transaksi, payment);
            } catch (ISOException e1) {
                e1.printStackTrace();
            }
            sendBack(ResponseCode.SERVER_UNAVAILABLE.setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM()));
        }

    }

    private List<Rules> parsingRules(ISOMsg d, boolean status) {
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(d.getString(48))
                .setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, status))
                .generate();
        legth = new SDE.Builder()
                .setPayload(d.getString(48))
                .setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, status))
                .calculate();
        String inquiryRes = d.getString(48);
        rincian = inquiryRes.substring(legth,inquiryRes.length());
        bit48.add(new Rules(d.getString(63)));
        return bit48;
    }

    private void sendBack(ResponseCode r){
        if(response.isSuspended()){
            response.resume(Response.status(200).entity(r).build());
        }
    }
    private void sendBack(Response r){
        if(response.isSuspended()){
            response.resume(r);
        } else {
            try {
                StringEntity entity = new StringEntity(objectMapper.writeValueAsString(r.getEntity()));
                httpPost.setEntity(entity);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                CloseableHttpResponse response = null;
                response = client.execute(httpPost);
                String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
