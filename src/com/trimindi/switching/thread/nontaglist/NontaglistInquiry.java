package com.trimindi.switching.thread.nontaglist;

import com.trimindi.switching.client.ChannelManager;
import com.trimindi.switching.models.*;
import com.trimindi.switching.response.nontaglist.Inquiry;
import com.trimindi.switching.response.nontaglist.InquiryResponse;
import com.trimindi.switching.services.PartnerDepositService;
import com.trimindi.switching.services.ProductFeeService;
import com.trimindi.switching.services.TransaksiService;
import com.trimindi.switching.thread.BaseThread;
import com.trimindi.switching.utils.TLog;
import com.trimindi.switching.utils.constanta.Constanta;
import com.trimindi.switching.utils.constanta.ResponseCode;
import com.trimindi.switching.utils.constanta.TStatus;
import com.trimindi.switching.utils.generator.NonTagListGenerator;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorNonTagList;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.NameRegistrar;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by HP on 15/05/2017.
 */
public class NontaglistInquiry extends BaseThread implements Runnable {
    private static final String LOG = NontaglistInquiry.class.getSimpleName();
    private Map<String, String> params;
    private AsyncResponse response;
    private ChannelManager channelManager;
    private PartnerCredential partnerCredential;
    private PartnerDepositService partnerDepositService;
    private TransaksiService transaksiService;
    private ProductFeeService productFeeService;
    private ProductItem productItem;

    public NontaglistInquiry(AsyncResponse asyncResponse, Map<String, String> request, PartnerCredential partnerCredential, final ProductItem productItem) {
        super();
        try {
            this.productItem = productItem;
            this.partnerCredential = partnerCredential;
            this.partnerDepositService = new PartnerDepositService();
            this.transaksiService = new TransaksiService();
            this.params = request;
            this.productFeeService = new ProductFeeService();
            this.response = asyncResponse;
            this.channelManager = (ChannelManager) NameRegistrar.get("manager");
            response.setTimeoutHandler(new TimeoutHandler() {
                @Override
                public void handleTimeout(AsyncResponse asyncResponse) {
                    sendBack(ResponseCode.SERVER_TIMEOUT.setProduct(productItem.getDENOM()));
                }
            });
            response.setTimeout(60, TimeUnit.SECONDS);
        } catch (NameRegistrar.NotFoundException e) {
            sendBack(Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(ResponseCode.SERVER_UNAVAILABLE.setProduct(productItem.getDENOM())).build());
        }
    }

    @Override
    public void run() {
        super.run();
        ISOMsg inquiry;
        try {
            inquiry = channelManager.sendMsg(NonTagListGenerator.generateInquiry(params.get(Constanta.MACHINE_TYPE),params.get(Constanta.MSSIDN)));
            if (inquiry != null) {
                if (inquiry.getString(39).equals("0000")) {
                    Transaksi transaksi = new Transaksi();
                    boolean status = true;
                    List<Rules> bit48 = new SDE.Builder()
                            .setPayload(inquiry.getString(48))
                            .setRules(ResponseRulesGeneratorNonTagList.nonTagListInquiryResponse(48, status))
                            .generate();
                    List<Rules> bit62 = new SDE.Builder()
                            .setPayload(inquiry.getString(62))
                            .setRules(ResponseRulesGeneratorNonTagList.nonTagListInquiryResponse(62, status))
                            .generate();

                    bit48.addAll(bit62);
                    System.out.print(bit48.toString());
                    Inquiry inquiryResponse = new Inquiry(bit48,true);
                    ProductFee productFee  = productFeeService.findFee(partnerCredential.getPartner_id(),params.get(Constanta.DENOM));
                    PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(partnerCredential.getPartner_id());
                    transaksi
                            .setADMIN(inquiryResponse.getAdmin())
                            .setMSSIDN_NAME(inquiryResponse.getSubscriberName())
                            .setBILL_REF_NUMBER(inquiryResponse.getBukopinReferenceNumber())
                            .setMSSIDN(inquiryResponse.getRegistrationNumber())
                            .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                            .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                            .setAMOUT(inquiryResponse.getTagihan())
                            .setFEE(productFee.getFEE())
                            .setUSERID(partnerCredential.getPartner_uid())
                            .setINQUIRY(new String(inquiry.pack()))
                            .setCHARGE(inquiryResponse.getAdmin() + inquiryResponse.getTagihan() - productFee.getFEE())
                            .setDEBET(inquiryResponse.getAdmin() + inquiryResponse.getTagihan() - productFee.getFEE())
                            .setMAC_ADDRESS(params.get(Constanta.MAC))
                            .setDENOM(productItem.getDENOM())
                            .setPRODUCT(productItem.getProduct_id())
                            .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                            .setPARTNERID(partnerCredential.getPartner_id())
                            .setPRODUCT(params.get(Constanta.PRODUCT_CODE))
                            .setLATITUDE(params.get(Constanta.LATITUDE))
                            .setLONGITUDE(params.get(Constanta.LONGITUDE))
                            .setST(TStatus.INQUIRY);
                    transaksiService.persist(transaksi);
                    InquiryResponse baseResponse = new InquiryResponse();
                    baseResponse.setData(inquiryResponse);
                    baseResponse.setFee(productFee.getFEE()).setNtrans(transaksi.getNTRANS());
                    baseResponse.setSaldo(partnerDeposit.getBALANCE());
                    baseResponse.setTotalFee(transaksiService.getTotalFee(partnerCredential.getPartner_id()));
                    baseResponse.setTagihan(transaksi.getAMOUT());
                    baseResponse.setTotalBayar(transaksi.getCHARGE());
                    baseResponse.setProduct(transaksi.getDENOM());
                    sendBack(
                            Response.status(200)
                                    .entity(baseResponse)
                                    .build()
                    );
                } else {
                    sendBack(
                            Response.status(200)
                                    .entity(responseCode.get(inquiry.getString(39)).setProduct(productItem.getDENOM()))
                                    .build()
                    );
                }
            }
        } catch (Exception e) {
            TLog.log(LOG + " - " + e.getMessage());
            sendBack(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setProduct(productItem.getDENOM())).build());
        }
    }
    private void sendBack(ResponseCode r){
        if(response.isSuspended()){
            response.resume(Response.status(200).entity(r).build());
        }
    }
    private void sendBack(Response r){
        if(response.isSuspended()){
            response.resume(r);
        }
    }

}
