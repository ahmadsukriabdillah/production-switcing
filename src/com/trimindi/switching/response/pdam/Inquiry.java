package com.trimindi.switching.response.pdam;

import com.trimindi.switching.rajabiller.response.Data;
import com.trimindi.switching.rajabiller.response.MethodResponse;
import com.trimindi.switching.rajabiller.response.Value;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by PC on 6/19/2017.
 */

@XmlRootElement
public class Inquiry {
    private String KODEPRODUK;
    private String WAKTU;
    private String IDPELANGGAN1;
    private String IDPELANGGAN2;
    private String IDPELANGGAN3;
    private String NAMAPELANGGAN;
    private String PERIODETAGIHAN;
    private double NOMINAL;
    private double BIAYAADMIN;
    private String UID;
    private String PIN;
    private String REF1;
    private String REF2;
    private String REF3;
    private String STATUS;
    private String KETERANGAN;
    private String SALDOTERPOTONG;
    private String SISASALDO;
    private String URLSTRUK;

    public Inquiry() {
    }

    public Inquiry(MethodResponse methodResponse) {
        Data d = methodResponse.getParams().getParam().getValue().getArray().getData();
        Value[] v = d.getValue();
        this.KODEPRODUK = v[0].getString();
        this.WAKTU = v[1].getString();
        this.IDPELANGGAN1 = v[2].getString();
        this.IDPELANGGAN2 = v[3].getString();
        this.IDPELANGGAN3 = v[4].getString();
        this.NAMAPELANGGAN = v[5].getString();
        this.PERIODETAGIHAN = v[6].getString();
        this.NOMINAL = Double.parseDouble(v[7].getString());
        this.BIAYAADMIN = Double.parseDouble(v[8].getString());
        this.UID = v[9].getString();
        this.PIN = v[10].getString();
        this.REF1 = v[11].getString();
        this.REF2 = v[12].getString();
        this.REF3 = v[13].getString();
        this.STATUS = v[14].getString();
        this.KETERANGAN = v[15].getString();
        this.SALDOTERPOTONG = v[16].getString();
        this.SISASALDO = v[17].getString();
        this.URLSTRUK = v[18].getString();
    }


    @XmlTransient
    public String getKODEPRODUK() {
        return KODEPRODUK;
    }

    public Inquiry setKODEPRODUK(String KODEPRODUK) {
        this.KODEPRODUK = KODEPRODUK;
        return this;
    }

    @XmlTransient
    public String getWAKTU() {
        return WAKTU;
    }

    public Inquiry setWAKTU(String WAKTU) {
        this.WAKTU = WAKTU;
        return this;
    }

    public String getIDPELANGGAN1() {
        return IDPELANGGAN1;
    }

    public Inquiry setIDPELANGGAN1(String IDPELANGGAN1) {
        this.IDPELANGGAN1 = IDPELANGGAN1;
        return this;
    }


    @XmlTransient
    public String getIDPELANGGAN2() {
        return IDPELANGGAN2;
    }

    public Inquiry setIDPELANGGAN2(String IDPELANGGAN2) {
        this.IDPELANGGAN2 = IDPELANGGAN2;
        return this;
    }

    @XmlTransient
    public String getIDPELANGGAN3() {
        return IDPELANGGAN3;
    }

    public Inquiry setIDPELANGGAN3(String IDPELANGGAN3) {
        this.IDPELANGGAN3 = IDPELANGGAN3;
        return this;
    }

    public String getNAMAPELANGGAN() {
        return NAMAPELANGGAN;
    }

    public Inquiry setNAMAPELANGGAN(String NAMAPELANGGAN) {
        this.NAMAPELANGGAN = NAMAPELANGGAN;
        return this;
    }

    public String getPERIODETAGIHAN() {
        return PERIODETAGIHAN;
    }

    public Inquiry setPERIODETAGIHAN(String PERIODETAGIHAN) {
        this.PERIODETAGIHAN = PERIODETAGIHAN;
        return this;
    }

    public double getNOMINAL() {
        return NOMINAL;
    }

    public Inquiry setNOMINAL(double NOMINAL) {
        this.NOMINAL = NOMINAL;
        return this;
    }

    public double getBIAYAADMIN() {
        return BIAYAADMIN;
    }

    public Inquiry setBIAYAADMIN(double BIAYAADMIN) {
        this.BIAYAADMIN = BIAYAADMIN;
        return this;
    }


    @XmlTransient
    public String getUID() {
        return UID;
    }


    public Inquiry setUID(String UID) {
        this.UID = UID;
        return this;
    }


    @XmlTransient
    public String getPIN() {
        return PIN;
    }

    public Inquiry setPIN(String PIN) {
        this.PIN = PIN;
        return this;
    }

    @XmlTransient
    public String getREF1() {
        return REF1;
    }

    public Inquiry setREF1(String REF1) {
        this.REF1 = REF1;
        return this;
    }

    @XmlTransient
    public String getREF2() {
        return REF2;
    }

    public Inquiry setREF2(String REF2) {
        this.REF2 = REF2;
        return this;
    }

    @XmlTransient
    public String getREF3() {
        return REF3;
    }

    public Inquiry setREF3(String REF3) {
        this.REF3 = REF3;
        return this;
    }

    @XmlTransient
    public String getSTATUS() {
        return STATUS;
    }

    public Inquiry setSTATUS(String STATUS) {
        this.STATUS = STATUS;
        return this;
    }

    public String getKETERANGAN() {
        return KETERANGAN;
    }

    public Inquiry setKETERANGAN(String KETERANGAN) {
        this.KETERANGAN = KETERANGAN;
        return this;
    }


    @XmlTransient
    public String getSALDOTERPOTONG() {
        return SALDOTERPOTONG;
    }

    public Inquiry setSALDOTERPOTONG(String SALDOTERPOTONG) {
        this.SALDOTERPOTONG = SALDOTERPOTONG;
        return this;
    }


    @XmlTransient
    public String getSISASALDO() {
        return SISASALDO;
    }

    public Inquiry setSISASALDO(String SISASALDO) {
        this.SISASALDO = SISASALDO;
        return this;
    }

    public String getURLSTRUK() {
        return URLSTRUK;
    }

    public Inquiry setURLSTRUK(String URLSTRUK) {
        this.URLSTRUK = URLSTRUK;
        return this;
    }
}
