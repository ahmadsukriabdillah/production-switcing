package com.trimindi.switching.response.pulsa;

import com.trimindi.switching.rajabiller.response.Data;
import com.trimindi.switching.rajabiller.response.MethodResponse;
import com.trimindi.switching.rajabiller.response.Value;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by PC on 6/20/2017.
 */
@XmlRootElement
public class Payment {
    private String KODEPRODUK;
    private String WAKTU;
    private String NOHP;
    private String UID;
    private String PIN;
    private String SN;
    private String REF1;
    private String REF2;
    private String STATUS;
    private String KETERANGAN;
    private String SALDOTERPOTONG;
    private String SISASALDO;
    public Payment() {
    }

    public Payment(MethodResponse methodResponse) {
        Data d = methodResponse.getParams().getParam().getValue().getArray().getData();
        Value[] v = d.getValue();
        this.KODEPRODUK = v[0].getString();
        this.WAKTU = v[1].getString();
        this.NOHP = v[2].getString();
        this.UID = v[3].getString();
        this.PIN = v[4].getString();
        this.SN = v[5].getString();
        this.REF1 = v[6].getString();
        this.REF2 = v[7].getString();
        this.STATUS = v[8].getString();
        if (this.SN.isEmpty()) {
            this.KETERANGAN = "Pengisian ke nomor " + this.NOHP + " Sedang di proses";
        } else {
            this.KETERANGAN = "Pengisian ke nomor " + this.NOHP + " Berhasil";
        }
        this.SALDOTERPOTONG = v[10].getString();
        this.SISASALDO = v[11].getString();
    }

    public String getKODEPRODUK() {
        return KODEPRODUK;
    }

    public Payment setKODEPRODUK(String KODEPRODUK) {
        this.KODEPRODUK = KODEPRODUK;
        return this;
    }

    public String getWAKTU() {
        return WAKTU;
    }

    public Payment setWAKTU(String WAKTU) {
        this.WAKTU = WAKTU;
        return this;
    }

    public String getNOHP() {
        return NOHP;
    }

    public Payment setNOHP(String NOHP) {
        this.NOHP = NOHP;
        return this;
    }

    @XmlTransient
    public String getUID() {
        return UID;
    }

    public Payment setUID(String UID) {
        this.UID = UID;
        return this;
    }

    @XmlTransient
    public String getPIN() {
        return PIN;
    }

    public Payment setPIN(String PIN) {
        this.PIN = PIN;
        return this;
    }

    public String getSN() {
        return SN;
    }

    public Payment setSN(String SN) {
        this.SN = SN;
        return this;
    }

    @XmlTransient
    public String getREF1() {
        return REF1;
    }

    public Payment setREF1(String REF1) {
        this.REF1 = REF1;
        return this;
    }

    @XmlTransient
    public String getREF2() {
        return REF2;
    }

    public Payment setREF2(String REF2) {
        this.REF2 = REF2;
        return this;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public Payment setSTATUS(String STATUS) {
        this.STATUS = STATUS;
        return this;
    }

    public String getKETERANGAN() {
        return KETERANGAN;
    }

    public Payment setKETERANGAN(String KETERANGAN) {
        this.KETERANGAN = KETERANGAN;
        return this;
    }

    @XmlTransient
    public String getSALDOTERPOTONG() {
        return SALDOTERPOTONG;
    }

    public Payment setSALDOTERPOTONG(String SALDOTERPOTONG) {
        this.SALDOTERPOTONG = SALDOTERPOTONG;
        return this;
    }

    @XmlTransient
    public String getSISASALDO() {
        return SISASALDO;
    }

    public Payment setSISASALDO(String SISASALDO) {
        this.SISASALDO = SISASALDO;
        return this;
    }
}
