package com.trimindi.switching.response;

import com.trimindi.switching.response.prepaid.Inquiry;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * Created by PC on 6/12/2017.
 */
@XmlRootElement
public class BaseResponse<T extends Serializable> implements Serializable  {
    private boolean status = true;
    private String ntrans;
    private String trx_ca;
    private double tagihan;
    private double fee;
    private double totalBayar;
    private double saldo;
    private double totalFee;
    @XmlElement(name = "data")
    private T data;

    public BaseResponse() {
    }

    public String getTrx_ca() {
        return trx_ca;
    }

    public BaseResponse setTrx_ca(String trx_ca) {
        this.trx_ca = trx_ca;
        return this;
    }

    public boolean isStatus() {
        return status;
    }

    public BaseResponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public double getFee() {
        return fee;
    }

    public BaseResponse setFee(double fee) {
        this.fee = fee;
        return this;
    }

    public String getNtrans() {
        return ntrans;
    }

    public BaseResponse setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public BaseResponse setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    public double getTagihan() {
        return tagihan;
    }

    public BaseResponse setTagihan(double tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public double getTotalBayar() {
        return totalBayar;
    }

    public BaseResponse setTotalBayar(double totalBayar) {
        this.totalBayar = totalBayar;
        return this;
    }

    public double getTotalFee() {
        return totalFee;
    }

    public BaseResponse setTotalFee(double totalFee) {
        this.totalFee = totalFee;
        return this;
    }

    public T getData() {
        return data;
    }

    public BaseResponse setData(T data) {
        this.data = data;
        return this;
    }
}
