package com.trimindi.switching.services;

import com.trimindi.switching.SessionUtils;
import com.trimindi.switching.models.TransaksiState;
import org.hibernate.Criteria;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by PC on 6/11/2017.
 */
public class TransactionStatusService {


    public TransactionStatusService() {
    }

    public List<TransaksiState> findAll() {
        Session session = SessionUtils.getSession();
        Criteria criteria = session.createCriteria(TransaksiState.class);
        List<TransaksiState> q = criteria.list();
        session.close();
        return q;
    }
}
