package com.trimindi.switching.services;

import com.trimindi.switching.SessionUtils;
import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.utils.constanta.TStatus;
import com.trimindi.switching.models.PartnerDeposit;
import org.hibernate.Session;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import java.sql.Timestamp;

/**
 * Created by HP on 19/05/2017.
 */
public class PartnerDepositService {
    public PartnerDepositService(){
    }
    public boolean bookingSaldo(Transaksi t){
        try{
            Session session = SessionUtils.getSession();
            session.getTransaction().begin();
            PartnerDeposit p = session.get(PartnerDeposit.class,t.getPARTNERID());
            if(p.getBALANCE() < t.getCHARGE()){
                return false;
            }
            double saldo = p.getBALANCE() - t.getCHARGE();
            p.setMASUK(0.00);
            p.setBALANCE(saldo);
            p.setKELUAR(p.getKELUAR() + t.getCHARGE());
            session.update(p);
            session.getTransaction().commit();
            session.getTransaction().begin();
            t.setBALANCE(saldo);
            t.setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
            session.update(t);
            session.getTransaction().commit();
            session.close();

        }catch (Exception e){
            return false;
        }
        return true;
    }


    public PartnerDeposit findPartnerDeposit(String partner_id) {
        Session session = SessionUtils.getSession();
        PartnerDeposit partnerDeposit = session.get(PartnerDeposit.class,partner_id);
        session.close();
        return partnerDeposit;
    }

    public void reverse(Transaksi t, ISOMsg isoMsg) throws ISOException {
        Session session = SessionUtils.getSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        String payment = null;
        if(isoMsg != null){
            payment = new String(isoMsg.pack());
        }
        PartnerDeposit p = session.get(PartnerDeposit.class,t.getPARTNERID());
        double saldo = p.getBALANCE() + t.getCHARGE();
        p.setBALANCE(saldo);
        p.setKELUAR(p.getKELUAR() - t.getCHARGE());
        session.update(p);
        t.setPAYMENT(payment);
        t.setKREDIT(t.getCHARGE());
        t.setST(TStatus.PAYMENT_FAILED);
        session.update(t);
        tx.commit();
        session.close();
    }
    public void reverse(Transaksi t,String response){
        Session session = SessionUtils.getSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        PartnerDeposit p = session.get(PartnerDeposit.class,t.getPARTNERID());
        double saldo = p.getBALANCE() + t.getCHARGE();
        p.setBALANCE(saldo);
        p.setKELUAR(p.getKELUAR() - t.getCHARGE());
        session.update(p);
        t.setPAYMENT(response);
        t.setKREDIT(t.getCHARGE());
        t.setST(TStatus.PAYMENT_FAILED);
        session.update(t);
        tx.commit();
        session.close();
    }
}
