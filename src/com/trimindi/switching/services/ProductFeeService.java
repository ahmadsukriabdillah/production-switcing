package com.trimindi.switching.services;

import com.trimindi.switching.SessionUtils;
import com.trimindi.switching.models.ProductFee;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Created by PC on 6/19/2017.
 */
public class ProductFeeService {
    public ProductFeeService() {
    }

    public ProductFee findFee(String partnerid, String product_item) {
        Session session = SessionUtils.getSession();
        Criteria criteria = session.createCriteria(ProductFee.class);
        criteria.add(Restrictions.eq("PARTNER_ID",partnerid));
        criteria.add(Restrictions.eq("PRODUCT_ITEM",product_item));
        ProductFee p = (ProductFee) criteria.uniqueResult();
        session.close();
        return p;
    }
}
