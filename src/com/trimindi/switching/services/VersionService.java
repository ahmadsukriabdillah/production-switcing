package com.trimindi.switching.services;

import com.trimindi.switching.SessionUtils;
import com.trimindi.switching.models.Version;
import org.hibernate.Session;

/**
 * Created by HP on 21/05/2017.
 */
public class VersionService {
    public VersionService() {
    }
    public Version find(String mt){
        Session session = SessionUtils.getSession();
        Version v = session.get(Version.class,mt);
        session.close();
        return v;
    }
}
