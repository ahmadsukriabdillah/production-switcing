package com.trimindi.switching.services;

import com.trimindi.switching.SessionUtils;
import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.utils.constanta.TStatus;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by sx on 14/05/17.
 */
public class TransaksiService {

    public TransaksiService() {

    }


    public void persist(Transaksi entity) {
        Session session = SessionUtils.getSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        session.persist(entity);
        tx.commit();
        session.close();
    }

    public void update(Transaksi entity) {
        Session session = SessionUtils.getSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        session.update(entity);
        tx.commit();
        session.close();
    }

    public Transaksi findById(String id) {
        Session session = SessionUtils.getSession();
        Transaksi tx = (Transaksi) session.get(Transaksi.class,id);
        session.close();
        return tx;
    }

    public Transaksi findByMSSIDN(String mssidn){
        Session session = SessionUtils.getSession();
        Timestamp time = new Timestamp(System.currentTimeMillis());
        Timestamp timeEnd = new Timestamp(time.getTime());
        timeEnd.setTime(time.getTime() + 2 * 60 * 1000);
        Criteria criteria = session.createCriteria(Transaksi.class);
        criteria.add(Restrictions.eq("MSSIDN",mssidn));
        criteria.add(Restrictions.between("TIME_INQUIRY",time,timeEnd));
        Transaksi transaksi = (Transaksi) criteria.uniqueResult();
        session.close();
        return transaksi;
    }

    public Transaksi findByHostRefId(String refid){
        Session session = SessionUtils.getSession();
        Criteria criteria = session.createCriteria(Transaksi.class);
        criteria.add(Restrictions.eq("HOST_REF_NUMBER",refid));
        Transaksi transaksi = (Transaksi) criteria.uniqueResult();
        session.close();
        return transaksi;
    }


    public void updateToUnderProsess(Transaksi transaksi) {

        Session session = SessionUtils.getSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        transaksi.setST(TStatus.PAYMENT_PROSESS);
        session.update(transaksi);
        tx.commit();
        session.close();
    }
    public boolean check_if_transaksi_under_progress(String mssidn,String denom) {
        Session session = SessionUtils.getSession();
        Criteria criteria = session.createCriteria(Transaksi.class);
        criteria.add(Restrictions.eq("MSSIDN",mssidn));
        criteria.add(Restrictions.eq("DENOM",denom));
        criteria.add(Restrictions.eq("DATE",new Date(System.currentTimeMillis())));
        criteria.add(Restrictions.eq("ST",TStatus.PAYMENT_PROSESS));
        List<Transaksi> transaksi = criteria.list();
        session.close();
        return transaksi.size() == 0;
    }

    public List<Transaksi> findByBulanDanTahun(String bulan, String tahun, String partnerid, String st, String produt) {
        Session session = SessionUtils.getSession();
        Criteria criteria = session.createCriteria(Transaksi.class);
        if(!bulan.equals("SEMUA")){
            criteria.add(Restrictions.eq("MONTH",bulan));
        }
        if(!tahun.equals("SEMUA")){
            criteria.add(Restrictions.eq("YEAR",tahun));
        }
        if(!st.equals("SEMUA")){
            criteria.add(Restrictions.eq("ST",st));
        }
        if(!produt.equals("SEMUA")){
            criteria.add(Restrictions.eq("PRODUCT",produt));
        }
        criteria.add(Restrictions.eq("USERID",partnerid));
        List<Transaksi> transaksi = criteria.list();
        session.close();
        return transaksi;
    }

    public double getTotalFee(String partnerid) {
        Session session = SessionUtils.getSession();
        Criteria cr = session.createCriteria(Transaksi.class);
        cr.add(Restrictions.eq("PARTNERID",partnerid));
        cr.add(Restrictions.eq("ST",TStatus.PAYMENT_SUCCESS));
        cr.setProjection(Projections.sum("FEE"));
        if(cr.uniqueResult() != null){
            double r = (double) cr.uniqueResult();
            session.close();
            return r;
        }
        session.close();
        return 0.0;
    }

    public String generateNtrans(){
        Session s = SessionUtils.getSession();
        boolean status = true;
        String ntrans;
        Transaksi t;
        do {
            ntrans = UUID.randomUUID().toString().replaceAll("-", "");
            t = s.get(Transaksi.class,ntrans);
            if(t == null){
                s.close();
                status = false;
            }
        }while (status);
        return ntrans;
    }
    public String generateNtrans(int lenth){
        Session s = SessionUtils.getSession();
        boolean status = true;
        String ntrans;
        Transaksi t;
        do {
            ntrans = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase().substring(0,lenth);
            t = s.get(Transaksi.class,ntrans);
            if(t == null){
                s.close();
                status = false;
            }
        }while (status);
        return ntrans;
    }

}
