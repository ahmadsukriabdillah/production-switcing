package com.trimindi.switching.services;

import com.trimindi.switching.SessionUtils;
import com.trimindi.switching.models.PartnerCredential;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 * Created by HP on 14/05/2017.
 */
public class PartnerCredentialService {
    public PartnerCredentialService() {

    }

    public PartnerCredential findByUserId(String id) {
        Session session = SessionUtils.getSession();
        Criteria criteria = session.createCriteria(PartnerCredential.class);
        criteria.add(Restrictions.eq("partner_uid",id));

        PartnerCredential partnerCredential = (PartnerCredential) criteria.uniqueResult();
        session.close();
        return partnerCredential;
    }

    public void update(PartnerCredential partnerCredential) {
        Session session = SessionUtils.getSession();
        Transaction tx = session.beginTransaction();
        session.update(partnerCredential);
        tx.commit();
        session.close();
    }
}
