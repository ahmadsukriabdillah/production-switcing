package com.trimindi.switching.services;

import com.trimindi.switching.SessionUtils;
import com.trimindi.switching.models.Merchant;
import org.hibernate.Session;

/**
 * Created by HP on 20/05/2017.
 */
public class MerchantService {
    public MerchantService(){}

    public boolean isAvailabel(String merchantid){
        Session session = SessionUtils.getSession();
        Merchant merchant = session.get(Merchant.class,merchantid);
        session.close();
        return (merchant != null);
    }

    public Merchant find(String mt) {
        Session session = SessionUtils.getSession();
        Merchant merchant = session.get(Merchant.class,mt);
        session.close();
        return merchant;
    }
}
