package com.trimindi.switching.services;

import com.trimindi.switching.SessionUtils;
import com.trimindi.switching.models.Partner;
import org.hibernate.Session;

/**
 * Created by HP on 14/05/2017.
 */
public class PartnerService {

    public PartnerService(){
    }

    public Partner findById(String partnerid){
        Session session = SessionUtils.getSession();
        Partner p = (Partner) session.get(Partner.class,partnerid);
        session.close();
        return p;
    }
}
