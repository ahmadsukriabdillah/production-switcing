package com.trimindi.switching.rajabiller.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/18/2017.
 */
@XmlRootElement
public class Struct
{
    private Member[] member;

    public Member[] getMember ()
    {
        return member;
    }

    public void setMember (Member[] member)
    {
        this.member = member;
    }

    public Struct() {
    }

    @Override
    public String toString()
    {
        return "ClassPojo [member = "+member+"]";
    }
}