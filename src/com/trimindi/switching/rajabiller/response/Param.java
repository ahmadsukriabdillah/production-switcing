package com.trimindi.switching.rajabiller.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/18/2017.
 */
@XmlRootElement
public class Param
{
    private Value value;

    public Value getValue ()
    {
        return value;
    }

    public void setValue (Value value)
    {
        this.value = value;
    }

    public Param() {
    }

    @Override
    public String toString()
    {
        return "ClassPojo [value = "+value+"]";
    }
}

