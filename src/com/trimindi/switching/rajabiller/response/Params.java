package com.trimindi.switching.rajabiller.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/18/2017.
 */
@XmlRootElement
public class Params
{
    private Param param;

    public Param getParam ()
    {
        return param;
    }

    public void setParam (Param param)
    {
        this.param = param;
    }

    public Params() {
    }

    @Override
    public String toString()
    {
        return "ClassPojo [param = "+param+"]";
    }
}