package com.trimindi.switching.rajabiller.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/18/2017.
 */
@XmlRootElement
public class MethodResponse
{
    private Params params;

    public Params getParams ()
    {
        return params;
    }

    public MethodResponse() {
    }

    public void setParams (Params params)
    {
        this.params = params;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [params = "+params+"]";
    }
}
