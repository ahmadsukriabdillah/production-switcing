package com.trimindi.switching.rajabiller;

import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.rajabiller.response.MethodResponse;
import com.trimindi.switching.response.pdam.Inquiry;
import com.trimindi.switching.utils.TLog;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

/**
 * Created by PC on 6/19/2017.
 */
public class RajabilerRequest {
    private static final String UID = "SP90212";
    private static final String PIN = "241189";
    private static JAXBContext jc;
    private static Unmarshaller unmarshaller;

    public static String inquiryPDAM(String kodeproduk, String idpel1, String idpel2, String idpel3, String ntrans){
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.inq</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        return String.format(raw,kodeproduk,idpel1,idpel2,idpel3,UID,PIN,ntrans);
    }

    public static String paymentPDAM(Transaksi transaksi){
        try {
            jc = JAXBContext.newInstance(MethodResponse.class);
            unmarshaller = jc.createUnmarshaller();
        } catch (JAXBException e) {
            TLog.log("ERRR" + e.getMessage());
        }
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.paydetail</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        int nominal = (int) (transaksi.getAMOUT() + transaksi.getADMIN());
        StringReader stringReader = new StringReader(transaksi.getINQUIRY());
        try {
            MethodResponse methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
            Inquiry inquiry = new Inquiry(methodResponse);
            return String.format(raw, transaksi.getDENOM(), transaksi.getMSSIDN(), transaksi.getMSSIDN(), transaksi.getMSSIDN(), Integer.toString(nominal), UID, PIN, transaksi.getNTRANS(), inquiry.getREF2(), inquiry.getPERIODETAGIHAN());
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String buyPulsa(String kodeproduk,String nohp,String ntrans){
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.pulsa</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        return String.format(raw,kodeproduk,nohp,UID,PIN,ntrans);
    }

    public static String inquiryTelkom(String kodeproduk,String area, String idpel, String ntrans,boolean speedy){
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.inq</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        if(speedy){
            return String.format(raw,kodeproduk,idpel,"","",UID,PIN,ntrans);
        }else{
            return String.format(raw,kodeproduk,area,idpel,"",UID,PIN,ntrans);
        }
    }

    public static String paymentTelokm(Transaksi transaksi){
        try {
            jc = JAXBContext.newInstance(MethodResponse.class);
            unmarshaller = jc.createUnmarshaller();
        } catch (JAXBException e) {
            TLog.log("ERRR" + e.getMessage());
        }
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.paydetail</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        int nominal = (int) (transaksi.getAMOUT() + transaksi.getADMIN());
        StringReader stringReader = new StringReader(transaksi.getINQUIRY());
        try {
            MethodResponse methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
            Inquiry inquiry = new Inquiry(methodResponse);
            if(transaksi.getDENOM().equalsIgnoreCase("SPEEDY")){
                return String.format(raw,transaksi.getDENOM(),inquiry.getIDPELANGGAN1(),inquiry.getIDPELANGGAN2(),inquiry.getIDPELANGGAN3(),inquiry.getNOMINAL(),UID,PIN,transaksi.getNTRANS(),inquiry.getREF2(),inquiry.getPERIODETAGIHAN());
            }else{
                return String.format(raw,transaksi.getDENOM(),inquiry.getIDPELANGGAN1(),inquiry.getIDPELANGGAN2(),inquiry.getIDPELANGGAN3(),inquiry.getNOMINAL(),UID,PIN,transaksi.getNTRANS(),inquiry.getREF2(),inquiry.getPERIODETAGIHAN());
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return "";
    }
}
