package com.trimindi.switching.servindo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.models.*;
import com.trimindi.switching.rajabiller.RajabilerRequest;
import com.trimindi.switching.rajabiller.response.MethodResponse;
import com.trimindi.switching.services.PartnerDepositService;
import com.trimindi.switching.services.ProductFeeService;
import com.trimindi.switching.services.ProductItemService;
import com.trimindi.switching.services.TransaksiService;
import com.trimindi.switching.utils.TLog;
import com.trimindi.switching.utils.constanta.Constanta;
import com.trimindi.switching.utils.constanta.ResponseCode;
import com.trimindi.switching.utils.constanta.TStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Objects;

/**
 * Created by PC on 21/07/2017.
 */
public class ServindoService {
    private static HttpGet httpGet;
    private static URIBuilder builder;
    private TransaksiService transaksiService;
    private ProductItemService productItemService;
    private ProductFeeService productFeeService;
    private PartnerDepositService partnerDepositService;
    private CloseableHttpClient client;
    private ObjectMapper objectMapper;
    private String host = "10.8.0.22";
    private int port = 2022;
    private String path = "/";
    private String userid = "trimata";
    private String password = "2020";

    public ServindoService() {
        builder = new URIBuilder();
        objectMapper = new ObjectMapper();
        SSLContext sslContext;
        try {
            sslContext = new SSLContextBuilder()
                    .loadTrustMaterial(null, new TrustStrategy() {
                        @Override
                        public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                            return true;
                        }
                    }).build();
            client = HttpClients.custom()
                    .setSSLContext(sslContext)
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            e.printStackTrace();
        }
        this.transaksiService = new TransaksiService();
        this.productItemService = new ProductItemService();
        this.productFeeService = new ProductFeeService();
        this.partnerDepositService = new PartnerDepositService();
    }

    public void beliPulsa(@Suspended  AsyncResponse toResponse, ProductItem product, Map<String, String> params, PartnerCredential partnerCredential) {
        Transaksi transaksi = null;
        String reg_id = transaksiService.generateNtrans(16);
        try {
            transaksi = new Transaksi()
                    .setADMIN(0)
                    .setMSSIDN_NAME(params.get(Constanta.MSSIDN))
                    .setHOST_REF_NUMBER(reg_id)
                    .setMSSIDN(params.get(Constanta.MSSIDN))
                    .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                    .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                    .setPRODUCT(product.getProduct_id())
                    .setAMOUT(product.getAMOUT())
                    .setDENOM(product.getDENOM())
                    .setFEE(0)
                    .setUSERID(partnerCredential.getPartner_uid())
                    .setCHARGE(product.getAMOUT())
                    .setDEBET(product.getAMOUT())
                    .setMAC_ADDRESS(params.get(Constanta.MAC))
                    .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                    .setPARTNERID(partnerCredential.getPartner_id())
                    .setLATITUDE(params.get(Constanta.LATITUDE))
                    .setLONGITUDE(params.get(Constanta.LONGITUDE))
                    .setPINALTY(0)
                    .setST(TStatus.PAYMENT_PROSESS);
            transaksiService.persist(transaksi);
            if(partnerDepositService.bookingSaldo(transaksi)){
                builder = new URIBuilder();
                builder.setScheme("http").setHost(host).setPort(port).setPath(path)
                        .setParameter("regid",reg_id )
                        .setParameter("userid", userid)
                        .setParameter("passwd", password)
                        .setParameter("msisd", params.get(Constanta.MSSIDN))
                        .setParameter("denom",product.getDENOM().substring(2));
                httpGet = new HttpGet(builder.build());
                System.out.println(builder.build());
                CloseableHttpResponse response = client.execute(httpGet);

                String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
                System.out.println(respone);
                TLog.log("BELI PULSA" + respone);
                PaketData paketData = objectMapper.readValue(respone,PaketData.class);
                paketData.setKodeproduk(product.getDENOM());
                PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(partnerCredential.getPartner_id());
                PaymentResponse paymentResponse = new PaymentResponse();
                switch (paketData.getStatus()) {
                    case 1:
                        transaksi.setST(TStatus.PAYMENT_SUCCESS)
                                .setPAYMENT(respone)
                                .setBILL_REF_NUMBER(paketData.getTn())
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        paymentResponse.setData(paketData);
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setSaldo(partnerDeposit.getBALANCE());
                        paymentResponse.setTotalFee(transaksiService.getTotalFee(partnerCredential.getPartner_id()));
                        paymentResponse.setTotalBayar(transaksi.getCHARGE());
                        paymentResponse.setProduct(transaksi.getDENOM());
                        toResponse.resume(Response.status(200).entity(paymentResponse).build());
                        break;
                    case 2:
                        transaksi.setST(TStatus.PAYMENT_PROSESS)
                                .setPAYMENT(respone)
                                .setBILL_REF_NUMBER(paketData.getTn())
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        paymentResponse.setData(paketData);
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setSaldo(partnerDeposit.getBALANCE());
                        paymentResponse.setTotalFee(transaksiService.getTotalFee(partnerCredential.getPartner_id()));
                        paymentResponse.setTotalBayar(transaksi.getCHARGE());
                        paymentResponse.setProduct(transaksi.getDENOM());
                        toResponse.resume(Response.status(200).entity(paymentResponse).build());
                        break;
                    default:
                        partnerDepositService.reverse(transaksi,respone);
                        toResponse.resume(Response.status(200).entity(new ResponseCode(String.valueOf(paketData.getStatus()),paketData.getMessage()).setProduct(product.getDENOM())).build());
                        break;
                }
            }else{
                toResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setProduct(product.getDENOM())).build());
            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            TLog.log("ERRR" + e.getMessage());
            partnerDepositService.reverse(transaksi,"");
            toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setProduct(product.getDENOM())).build());
        }
    }

    public void beliData(AsyncResponse asyncResponse, ProductItem productItem, Map<String, String> params, PartnerCredential p) {
        Transaksi transaksi = null;
        String reg_id = transaksiService.generateNtrans(16);
        try {
            transaksi = new Transaksi()
                    .setADMIN(0)
                    .setMSSIDN_NAME(params.get(Constanta.MSSIDN))
                    .setHOST_REF_NUMBER(reg_id)
                    .setMSSIDN(params.get(Constanta.MSSIDN))
                    .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                    .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                    .setPRODUCT(productItem.getProduct_id())
                    .setAMOUT(productItem.getAMOUT())
                    .setDENOM(productItem.getDENOM())
                    .setFEE(0)
                    .setUSERID(p.getPartner_uid())
                    .setCHARGE(productItem.getAMOUT())
                    .setDEBET(productItem.getAMOUT())
                    .setMAC_ADDRESS(params.get(Constanta.MAC))
                    .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                    .setPARTNERID(p.getPartner_id())
                    .setLATITUDE(params.get(Constanta.LATITUDE))
                    .setLONGITUDE(params.get(Constanta.LONGITUDE))
                    .setPINALTY(0)
                    .setST(TStatus.PAYMENT_PROSESS);
            transaksiService.persist(transaksi);
            if(partnerDepositService.bookingSaldo(transaksi)){
                builder = new URIBuilder();
                builder.setScheme("http").setHost(host).setPort(port).setPath(path)
                        .setParameter("regid",reg_id)
                        .setParameter("userid", userid)
                        .setParameter("passwd", password)
                        .setParameter("msisd", params.get(Constanta.MSSIDN))
                        .setParameter("denom",productItem.getDENOM());
                httpGet = new HttpGet(builder.build());
                CloseableHttpResponse response = client.execute(httpGet);

                String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
                System.out.println(respone);
                TLog.log("BELI PULSA" + respone);
                PaketData paketData = objectMapper.readValue(respone,PaketData.class);
                paketData.setKodeproduk(productItem.getDENOM());
                PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(p.getPartner_id());
                PaymentResponse paymentResponse = new PaymentResponse();
                switch (paketData.getStatus()) {
                    case 1:
                        transaksi.setST(TStatus.PAYMENT_SUCCESS)
                                .setPAYMENT(respone)
                                .setBILL_REF_NUMBER(paketData.getTn())
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        paymentResponse.setData(paketData);
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setSaldo(partnerDeposit.getBALANCE());
                        paymentResponse.setTotalFee(transaksiService.getTotalFee(p.getPartner_id()));
                        paymentResponse.setTotalBayar(transaksi.getCHARGE());
                        paymentResponse.setProduct(transaksi.getDENOM());
                        asyncResponse.resume(Response.status(200).entity(paymentResponse).build());
                        break;
                    case 2:
                        transaksi.setST(TStatus.PAYMENT_PROSESS)
                                .setPAYMENT(respone)
                                .setBILL_REF_NUMBER(paketData.getTn())
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        paymentResponse = new PaymentResponse();
                        paymentResponse.setData(paketData);
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setSaldo(partnerDeposit.getBALANCE());
                        paymentResponse.setTotalFee(transaksiService.getTotalFee(p.getPartner_id()));
                        paymentResponse.setTotalBayar(transaksi.getCHARGE());
                        paymentResponse.setProduct(transaksi.getDENOM());
                        asyncResponse.resume(Response.status(200).entity(paymentResponse).build());
                        break;
                    default:
                        partnerDepositService.reverse(transaksi,respone);
                        asyncResponse.resume(Response.status(200).entity(new ResponseCode(String.valueOf(paketData.getStatus()),paketData.getMessage()).setProduct(transaksi.getDENOM())).build());
                        break;
                }
            }else{
                asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setProduct(productItem.getDENOM())).build());
            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            TLog.log("ERRR" + e.getMessage());
            partnerDepositService.reverse(transaksi,"");
            asyncResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setProduct(productItem.getDENOM())).build());
        }
    }
}
