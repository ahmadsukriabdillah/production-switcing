package com.trimindi.switching.controllers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/17/2017.
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request {
    public String PRODUCT;
    public String ACTION;
    public String MSSIDN;
    public String NTRANS;
    public String BULAN;
    public String TAHUN;
    public String STATUS;
    public String AREA;
    public String BACK_LINK;
}
