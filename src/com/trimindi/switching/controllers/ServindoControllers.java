package com.trimindi.switching.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.RestListener;
import com.trimindi.switching.models.PartnerCredential;
import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.services.PartnerCredentialService;
import com.trimindi.switching.servindo.PaketData;
import com.trimindi.switching.servindo.PaymentResponse;
import com.trimindi.switching.utils.constanta.TStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOPackager;
import org.jpos.util.LogSource;

import javax.net.ssl.SSLContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Objects;

/**
 * Created by PC on 21/07/2017.
 */
@Path("servindo")
public class ServindoControllers extends RestListener implements LogSource, Configurable {


    private final ObjectMapper objectMapper;
    private HttpPost httpPost;
    private CloseableHttpClient client;
    private PartnerCredentialService partnerCredentialService;


    public ServindoControllers(ISOPackager packager) {
        super(packager);
        this.partnerCredentialService = new PartnerCredentialService();
        this.objectMapper = new ObjectMapper();
        SSLContext sslContext = null;
        try {
            sslContext = new SSLContextBuilder()
                    .loadTrustMaterial(null, new TrustStrategy() {
                        @Override
                        public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                            return true;
                        }
                    }).build();
            client = HttpClients.custom()
                    .setSSLContext(sslContext)
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .build();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

    }

    @POST
    @Path("report")
    public void report(@Suspended AsyncResponse asyncResponse, String response){
        System.out.println(response);
        PaketData paketData;
        try {
            paketData = objectMapper.readValue(response,PaketData.class);
            System.out.println(paketData);
            if(paketData != null){
                Transaksi transaksi = transaksiService.findByHostRefId(paketData.getReg_id());
                if (transaksi != null){
                    PartnerCredential partnerCredential = partnerCredentialService.findByUserId(transaksi.getUSERID());
                    httpPost = new HttpPost(partnerCredential.getBack_link());
                    paketData.setKodeproduk(transaksi.getDENOM());
                    PaymentResponse paymentResponse = new PaymentResponse();
                    if(paketData.getStatus() == 1){
                        transaksi.setST(TStatus.PAYMENT_SUCCESS)
                                .setPAYMENT(response)
                                .setBILL_REF_NUMBER(paketData.getSn());
                        transaksiService.update(transaksi);
                        asyncResponse.resume(Response.status(200).entity("OK").build());
                    }else if(paketData.getStatus() == 3){
                        transaksi.setST(TStatus.PAYMENT_FAILED)
                                .setPAYMENT(response)
                                .setBILL_REF_NUMBER(paketData.getSn());
                        transaksiService.update(transaksi);
                        partnerDepositService.reverse(transaksi,response);
                        asyncResponse.resume(Response.status(200).entity("OK").build());
                    }else{
                        asyncResponse.resume(Response.status(200).entity("OK").build());
                    }
                    paymentResponse.setStatus((paketData.getStatus() == 1) || paketData.getStatus() == 2);
                    paymentResponse.setData(paketData);
                    paymentResponse.setNtrans(transaksi.getNTRANS());
                    paymentResponse.setSaldo(partnerDepositService.findPartnerDeposit(partnerCredential.getPartner_uid()).getBALANCE());
                    paymentResponse.setTotalFee(transaksiService.getTotalFee(partnerCredential.getPartner_id()));
                    paymentResponse.setTotalBayar(transaksi.getCHARGE());
                    sendToMitra(paymentResponse);
                }else{
                    asyncResponse.resume(Response.status(200).entity("ERORR REG ID TIDAK DIKENALI").build());
                }
            }
        } catch (IOException e) {
            asyncResponse.resume(Response.status(200).entity("TERJADI KESALAHAN PADA SERVER").build());
        }
    }
    @Override
    public void setConfiguration(Configuration configuration) throws ConfigurationException {

    }

    private void sendToMitra(PaymentResponse response){
        try {
            StringEntity entity = new StringEntity(objectMapper.writeValueAsString(response));
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            CloseableHttpResponse r = null;
            r = client.execute(httpPost);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}